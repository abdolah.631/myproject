#!/usr/bin/env sh
set -e

composer install
composer dump-autoload
php artisan migrate
php artisan storage:link
php artisan key:generate
#php artisan passport:install
php artisan db:seed
