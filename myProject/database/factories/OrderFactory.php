<?php

namespace Database\Factories;

use App\Models\Biker;
use App\Models\File\File;
use App\Models\Order\Order;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Biker>
 */
class OrderFactory extends Factory
{

    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::query()->inRandomOrder()->first()->id ,
            'cost' => $this->faker->randomNumber(),
            'description' => $this->faker->text,
            'status' => $this->faker->randomElement(Order::$status),
            'payment_status' => $this->faker->randomElement(Order::$paymentStatus),
        ];
    }
}
