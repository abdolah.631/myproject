<?php

namespace Database\Factories;

use App\Models\File\File;
use App\Models\File\RootFile;
use Illuminate\Database\Eloquent\Factories\Factory;


class FileFactory extends Factory
{

    protected $model = File::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'root_file_id' => RootFile::factory()->create()->id,
            'name' => $this->faker->name,
            'extension' => $this->faker->name,
            'status' => $this->faker->randomElement(File::$statusTypes),
            'uuid' => $this->faker->uuid,
        ];
    }

}
