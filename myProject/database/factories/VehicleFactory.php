<?php

namespace Database\Factories;

use App\Models\Biker;
use App\Models\File\File;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User\User>
 */
class VehicleFactory extends Factory
{

    protected $model = Vehicle::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->colorName,
            'slug' => $this->faker->slug,
            'is_active' => $this->faker->boolean,
            'color' => $this->faker->hexColor,
            'year_of_construction' => $this->faker->randomNumber(),
            'plaque_number' =>  $this->faker->randomNumber(),
            'vehicle_card_file_id' =>File::factory()->create()->id,
            'biker_id' =>Biker::query()->inRandomOrder()->first()->id,

        ];
    }

}
