<?php

namespace Database\Factories;

use App\Models\File\RootFile;
use Illuminate\Database\Eloquent\Factories\Factory;


class RootFileFactory extends Factory
{

    protected $model = RootFile::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'path' => $this->faker->filePath(),
            'content_hash' => $this->faker->unique()->name,
            'mime_type' => $this->faker->mimeType(),
            'size' => $this->faker->buildingNumber,
        ];
    }

    /**
     * @return RootFileFactory
     */
    protected static function newFactory()
    {
        return new RootFileFactory();
    }

}
