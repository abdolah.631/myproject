<?php

namespace Database\Factories;

use App\Models\Biker;
use App\Models\File\File;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Biker>
 */
class BikerFactory extends Factory
{

    protected $model = Biker::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id ,
            'status' => $this->faker->randomElement(Biker::STATUS),
            'is_active' => $this->faker->boolean,
            'identity_card_file_id' => File::factory()->create()->id,
            'license_file_id' => File::factory()->create()->id,
            'registration_date' => $this->faker->dateTime,
        ];
    }
}
