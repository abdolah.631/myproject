<?php

namespace Database\Factories;

use App\Models\Biker;
use App\Models\File\File;
use App\Models\Order\Order;
use App\Models\Order\OrderBiker;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;


class OrderBikerFactory extends Factory
{

    protected $model = OrderBiker::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'order_id' => Order::query()->inRandomOrder()->first()->id ,
            'biker_id' => Biker::query()->inRandomOrder()->first()->id ,
            'status' => $this->faker->randomElement(OrderBiker::$status),
        ];
    }
}
