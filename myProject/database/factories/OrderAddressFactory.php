<?php

namespace Database\Factories;

use App\Models\Biker;
use App\Models\File\File;
use App\Models\Order\Order;
use App\Models\Order\OrderAddress;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Biker>
 */
class OrderAddressFactory extends Factory
{

    protected $model = OrderAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'order_id' => Order::factory()->create()->id,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'sender_address' => $this->faker->address,
            'recipient_address' => $this->faker->address,
            'recipient_phone' => $this->faker->randomNumber(),
            'sender_phone' => $this->faker->randomNumber(),
//            'begin_latitude ' => $this->faker->latitude,
//            'begin_longitude' => $this->faker->longitude,
//            'destination_latitude' => $this->faker->latitude,
//            'destination_longitude' => $this->faker->longitude,
        ];
    }
}
