<?php

namespace Database\Seeders;

use App\Constants\Permissions;
use App\Models\User\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classes = [
            new \ReflectionClass(Permissions::class),
        ];

        foreach ($classes as $class) {
            foreach ($class->getConstants() as $permission) {
                Permission::firstOrCreate(
                    [
                        'name' => $permission,
                        'guard_name' => 'web'
                    ]
                );
            }
        }
    }
}
