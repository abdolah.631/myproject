<?php

namespace Database\Seeders;

use App\Models\Biker;
use App\Models\User\Role;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersEmail = [
            'user@test.test' => Role::USER,
            'biker@test.test' => Role::BIKER,
            'admin@test.test' => Role::ADMIN,
        ];
        $password = bcrypt(12345678);

        foreach ($usersEmail as $email => $roleName) {
            $role = Role::getRoleByName($roleName);
            if (!$role) {
                throw new \Exception('please run RoleSeeder OR add role ' . $roleName);
            }

            $user = User::query()
                ->where('email', $email)
                ->first();

            if ($user) {
                $user->password = $password;
                $user->save();
            } else {
                $user = User::factory()
                    ->create(
                        [
                            'email' => $email,
                            'name' => $email,
                            'password' => $password,
                            'email_verified_at' => Carbon::now(),
                        ]
                    );
            }

            $user->roles()->attach($role->id);

            if ($roleName == Role::BIKER) {
                Biker::factory()->create([
                    'user_id' =>$user->id,
                ]);
            }
        }
    }
}
