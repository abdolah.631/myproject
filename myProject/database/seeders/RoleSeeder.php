<?php

namespace Database\Seeders;

use App\Models\User\Permission;
use App\Models\User\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::CUSTOM_ROLES;
        $defaultGuard = config('auth.defaults.guard');

        foreach ($roles as $permissionsClass => $role) {

            $role = Role::query()
                ->firstOrCreate(
                    [
                        'name' => $role,
                        'guard_name' => $defaultGuard,
                    ]
                );

            $permissions = $permissionsClass::PERMISSIONS;
            $permissionsIds = Permission::query()
                ->select(['id'])
                ->whereIn('name', $permissions)
                ->get()
                ->pluck('id')
                ->toArray();
            $role->permissions()->sync($permissionsIds);
        }
    }
}
