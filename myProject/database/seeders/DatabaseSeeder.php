<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Biker;
use App\Models\Order\Order;
use App\Models\Order\OrderAddress;
use App\Models\Order\OrderBiker;
use App\Models\User\User;
use App\Models\Vehicle;
use Database\Factories\OrderBikerFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Model::unsetEventDispatcher();

        User::factory()->create();
        Biker::factory()->create();
        Vehicle::factory()->create();
        OrderAddress::factory()->create();
        OrderBiker::factory()->create();


        $this->call([
            PermissionSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
        ]);

    }
}
