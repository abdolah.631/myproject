<?php

use App\Models\Biker;
use App\Models\File\File;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->boolean('is_active')->default(0);
            $table->date('registration_date');
            $table->foreignId('identity_card_file_id')->nullable()->constrained(File::TABLE);
            $table->foreignId('license_file_id')->nullable()->constrained(File::TABLE);
            $table->enum('status', Biker::STATUS);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bikers');
    }
};
