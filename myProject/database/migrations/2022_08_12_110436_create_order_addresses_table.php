<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_addresses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained('orders');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('sender_address');
            $table->string('recipient_address');
            $table->bigInteger('sender_phone');
            $table->bigInteger('recipient_phone');
            $table->integer('begin_latitude')->nullable();
            $table->integer('begin_longitude')->nullable();
            $table->integer('destination_latitude')->nullable();
            $table->integer('destination_longitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_addresses');
    }
};
