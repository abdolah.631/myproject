<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('is_active');
            $table->integer('plaque_number')->nullable();
            $table->integer('year_of_construction')->nullable();
            $table->foreignId('vehicle_card_file_id')->nullable()->constrained(\App\Models\File\File::TABLE);
            $table->foreignId('biker_id')->constrained('bikers','id');
            $table->string('color')->nullable();
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
};
