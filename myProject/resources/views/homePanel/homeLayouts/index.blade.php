<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
@include('homePanel.homeLayouts.heed')
  <!-- =======================================================
  * Template Name: Green - v4.8.0
  * Template URL: https://bootstrapmade.com/green-free-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body style="background-color: snow">

  <!-- ======= Top Bar ======= -->
 @include('homePanel.homeLayouts.topbar')

  <!-- ======= Header ======= -->
@include('homePanel.homeLayouts.header')
  <!-- End Header -->



  <main id="main">


      @yield('content')

  </main>

  <!-- ======= Footer ======= -->
  <footer id="footer">
  @include('homePanel.homeLayouts.footer')
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
@include('homePanel.homeLayouts.scriptFooter')
@yield('script')
</body>

</html>
