<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>Green Bootstrap Template - Index</title>
<meta content="" name="description">
<meta content="" name="keywords">

<!-- Favicons -->
<link href="{{asset('home')}}/assets/img/favicon.png" rel="icon">
<link href="{{asset('home')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<!-- Vendor CSS Files -->
<link rel="stylesheet" href="{{ asset('AdminPanelAssets')}}/dist/css/adminlte.min.css">
<link rel="stylesheet" href="{{ asset('AdminPanelAssets')}}/dist/css/custom-style.css">

<link href="{{asset('home')}}/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="{{asset('home')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="{{asset('home')}}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
<link href="{{asset('home')}}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
<link href="{{asset('home')}}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
<link href="{{asset('home')}}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="{{asset('home')}}/assets/css/style.css" rel="stylesheet">
