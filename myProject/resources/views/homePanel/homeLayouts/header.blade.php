<header id="header" class="d-flex align-items-center bg-info">
    <div class="container-fluid col-md-12 d-flex align-items-center ">

        <h1 class="logo col-md-3 "><a class="text-warning" href="#">اسنپ باکس</a></h1>

        <nav id="navbar" class="navbar d-flex justify-content-between col-md-9">
            <div>
                <ul>
                    <li><a class="nav-link scrollto  " href="{{route('home')}}">خانه</a></li>
                    {{-- user section--}}
                    @auth()
                        <li><a class="nav-link scrollto" href="{{route('make.order')}}">ایجاد سفر</a></li>
                        <li><a class="nav-link scrollto " href="{{route('user.order')}}">سفر های فعال</a></li>
                    @endauth

                    {{-- //biker section--}}
                    @if(auth()->user() && auth()->user()->is_biker)
                        <li><a class="nav-link scrollto" href="{{route('services')}}">سرویس ها</a></li>
                        <li><a class="nav-link scrollto" href="{{route('show.accepted.service')}}">سرویس انتخاب شده</a>
                        </li>
                    @endif
                    <li><a class="nav-link scrollto " href="#abuot">درباره اسنپ باکس</a></li>
                    <li><a class="nav-link scrollto" href="#team">تیم</a></li>
                </ul>
            </div>
            <div>
                <ul>
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item ">
                                <a class=" py-1 px-4 m-1 text-center text-primary  shadow-lg"
                                   href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif
                        @if (Route::has('register'))
                            <li class="nav-item m-auto">
                                <a type="button" class=" py-1 px-4 m-1 text-center text-primary shadow-lg  "
                                   href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="dropdown ">
                            <a href="#">
                                <span>{{ Auth::user()->name }}</span>
                                <i class="bi bi-chevron-down mr-2"></i>
                            </a>
                            <ul class="bg-primary rounded shadow-sm btn-outline-danger">
                                <li class="text-secondary">
                                    <a href="{{route('user.profile')}}" class="text-secondary">
                                        پروفایل
                                    </a>
                                </li>
                                <li class="text-secondary">
                                    <a href="{{ route('logout') }}" class="text-secondary" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">{{ __('Logout') }}
                                    </a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header>
