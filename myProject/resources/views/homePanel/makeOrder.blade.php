@extends('homePanel.homeLayouts.index')

@section('content')
    <section id="contact" class="contact">
        <div class="container">
            @include('AdminPanel.adminLayouts.errors')
            <div class="section-title">
                <h2>اسنپ</h2>
                <p class="text-danger">انتخاب وسیله نقلیه جهت اراسل مرسوله.</p>
            </div>
            <div id="map" style="width: 100%; height: 400px"></div>
            <div class="col-md-12  card">
                @csrf
                <div class="row card-body">
                    <div class="col-lg-6 d-flex align-items-stretch">
                        <div class="info">
                            <div class="address">
                                <i class="bi bi-geo-alt"></i>
                                <h4>موقعیت شما</h4>
                                <p>A108 Adam Street, New York, NY 535022</p>
                            </div>
                            <div class="address">
                                <i class="bi bi-geo-alt"></i>
                                <h4>موقعیت گیرنده</h4>
                                <p>A108 Adam Street, New York, NY 535022</p>
                            </div>
                            <div class="email">
                                <i class="bi bi-envelope"></i>
                                <h4>Email:</h4>
                                <p>info@example.com</p>
                            </div>
                            <div class="phone">
                                <i class="bi bi-phone"></i>
                                <h4>Call:</h4>
                                <p>+1 5589 55488 55s</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 card-body">
                        <div class="row ">
                            <div class="form-group col-md-6">
                                <input placeholder="نام" type="text" name="first_name"
                                       class="first_name form-control"
                                       id="first_name">
                            </div>
                            <div class="form-group col-md-6 ">
                                <input placeholder="نام خانوادگی" type="text" class="last_name form-control"
                                       name="last_name"
                                       id="last_name">
                            </div>
                        </div>
                        <div class="form-group ">
                            <input placeholder="تلفن فرستنده" type="number" class="phone form-control"
                                   name="sender_phone"
                                   id="sender_phone">
                        </div>
                        <div class="form-group ">
                            <input placeholder="آدرس فرستنده" type="text" class="form-control sender_address"
                                   name="sender_address"
                                   id="sender_address">
                        </div>
                        <div class="form-group ">
                            <input placeholder="آدرس گیرنده" type="text" class="form-control recipient_address"
                                   name="recipient_address" id="recipient_address">
                        </div>
                        <div class="form-group ">
                            <input placeholder="تلفن گیرنده" type="text" class="form-control recipient_phone"
                                   name="recipient_phone"
                                   id="recipient_phone">
                        </div>
                        <div class="form-group ">
                            <textarea class="form-control description" name="description" rows="5" placeholder="توضیحات"></textarea>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label mx-4" for="flexCheckDefault">
                                بیمه باکس
                            </label>
                        </div>
                        {{--                        <div class="my-1">--}}
                        {{--                            <div class="loading">Loading</div>--}}
                        {{--                            <div class="error-message"></div>--}}
                        {{--                            <div class="sent-message">Your snap has been search. Thank you!</div>--}}
                        {{--                        </div>--}}
                        <div class="text-center mb-2">
                            <span class="error" style="color:red; margin-top:10px; margin-bottom: 20px;"></span>
                            <button class="btn btn-info" id="submit" data-target="#exampleModalCenter">
                                جستجوی اسنپ باکس
                                <i class="fa-solid fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="{{route('search.biker')}}" class="btn btn-warning"> search</a>
        <button type="button" class="btn btn-primary" onclick="AutocompleteDirectionsHandler()"
                data-target="#exampleModalCenter">
            Launch demo modal
        </button>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body d-flex justify-content-around">
                        <span>در حال جستجو راننده ... </span>
                        <div class="spinner-border m-1" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary " id="close" data-dismiss="modal">لغو</button>
                        {{--                        <button type="button" class="btn btn-primary">Save changes</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoT29owl2VGw1x7Z8ioWgg0aCR56JfOPk&callback=initMap"></script>

    <script>
        $("#submit").click(async function (event) {
            event.preventDefault();

            const first_name = $("input[name=first_name]").val();
            const last_name = $("input[name=last_name]").val();
            const sender_phone = $("input[name=sender_phone]").val();
            const recipient_phone = $("input[name=recipient_phone]").val();
            const sender_address = $("input[name=sender_address]").val();
            const recipient_address = $("input[name=recipient_address]").val();
            const description = $("textarea[name=description]").val();
            const _token = $('meta[name="csrf-token"]').attr('content');

            const result = $.ajax({
                url: "{{route('order.create')}}",
                type: "POST",
                data: {
                    first_name: first_name,
                    last_name: last_name,
                    sender_phone: sender_phone,
                    recipient_phone: recipient_phone,
                    sender_address: sender_address,
                    recipient_address: recipient_address,
                    description: description,
                    _token: _token
                },
                success: function (response) {
                    if (response) {
                        $('#exampleModalCenter').modal('show');
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error)
                        $('.error').text(error.responseJSON.message);
                    }
                }
            });
        });

        $("#close").click(function (event) {
            event.preventDefault();
            $('#exampleModalCenter').modal('hide');
        });

        async function searchBiker() {
            await $.ajax({
                url: "{{route('search.biker')}}",
                type: "get",
                success: function (response) {
                    if (response) {
                        console.log(response)
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error)
                        $('.error').text(error.responseJSON.message);
                    }
                }
            });
        }

         let map;
         function initMap() {
             myOptions =
                 {
                     center: {lat: -34.397, lng: 150.644},
                     zoom: 6,
                     mapTypeId: google.maps.MapTypeId.ROADMAP
                 }
             map = new google.maps.Map(document.getElementById("map"), myOptions)
         }

         function AutocompleteDirectionsHandler() {
            let directionService = new google.maps.DirectionsService()
            let directionDisplay = new google.maps.DirectionsRenderer()

            directionDisplay.setMap(map)

            const request = {
                origin: $('#sender_address').val(),
                destination: $('#recipient_address').val(),
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.IMPERIAL
            }
            directionService.route(request, (result, status) => {
                if (status === google.maps.DirectionsStatus.OK) {
                    console.log(result, status)
                }
            })
        }

    //autocomplete map
        //         const options = {
        //             types: ['(cities)']
        //         }
        //
        //         let input1 = $('#sender_address');
        //         autocomplate1 = new google.maps.places.Autocomplete(input1, options);
        //
        //         console.log(autocomplate1)
        //         let input2 = $('#recipient_address');
        //         autocomplate2 = new google.maps.places.Autocomplete(input2, options);


        //pusher
        const beamsClient = new PusherPushNotifications.Client({
            instanceId: 'ea8afa61-815b-4c48-8b60-06d208332881',
        });

        beamsClient.start()
            .then(() => beamsClient.addDeviceInterest('hello'))
            .then(() => console.log('Successfully registered and subscribed!'))
            .catch(console.error);


    </script>
@endsection
