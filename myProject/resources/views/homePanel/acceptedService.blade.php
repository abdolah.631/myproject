@extends('homePanel.homeLayouts.index')

@section('content')
    <section id="sevices" class="content">
        @include('AdminPanel.adminLayouts.errors')
        <div class="container-fluid">
            <div class="row justify-content-center ">
                <h2 class="mb-2 text-center">سفر پذیرفته شده </h2>
                <div class="card text-black text-center  my-2 mx-2 "
                     style="max-width: 40rem; background-color: #ffb733;">
                    <span class="text-sm">شماره ({{$order->id }})</span>
                    <div class="card-header"> نام : {{$order->addresses->full_name }}</div>
                    <div class="card-body ">
                        <h5 class="card-title"> ادرس فرستنده :</h5>
                        <div class="bg-warning rounded col-12">
                            <p class="card-text">{{$order->addresses->sender_address}}</p>
                        </div>
                        <hr>
                        <h5 class="card-title"> ادرس گیرنده : </h5>
                        <div class="bg-warning rounded col-12">
                            <p class="card-text">{{$order->addresses->recipient_address }}</p>
                        </div>
                        <hr>
                        <h5 class="card-title"> تلفن گیرنده : </h5>
                        <div class="bg-warning rounded col-12">
                            <p class="card-text">{{$order->addresses->recipient_address }}</p>
                        </div>
                        <h5 class="card-title"> وضعیت : </h5>
                        <div class="bg-warning rounded col-12">
                            <p class="card-text">{{$order->status }}</p>
                        </div>
                        <hr>
                        <h5 class="card-title">توضیحات : </h5>
                        <div class="bg-warning rounded col-12">
                            <p class="card-text">{{$order->description}}</p>
                        </div>
                    </div>
                    <div>
                        @if($order->status !== 'load-receive')
                            <a href="{{route('cansel.service', ['order' => $order->id])}}" type="button"
                               id="cancel_button"
                               class="btn btn-danger col-3 mx-3 ">لغو سفر</a>
                        @endif

                        @csrf

                        <span class="error"></span>
                        @switch($order->status)
                            @case('biker-choices')
                                <input type="button" id="submit_status" data-status="load-pickup"
                                       class="btn btn-success col-3 mx-3" value="دریافت بسته">
                                @break
                            @case('load-pickup')
                                <input type="button" id="submit_status" data-status="load-receive"
                                       class="btn btn-success col-3 mx-3" value="تحویل بسته">
                                @break
                            @case('load-receive')
                                <span>سفر با موفقیت به پایان رسیده است </span>
                                @break
                        @endswitch
                        <div class="m-1">
                            <span class="error"></span>
                            <span class="success m-4 p-4 text-success  "></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@endsection

@section('script')
    <script>
        $("#submit_status").click(function (event) {
            event.preventDefault();

            const order_status = $('#submit_status').attr('data-status');
            const _token = '{{csrf_token()}}';

            const result = $.ajax({
                url: "{{route('change.order.status', ['order' => $order->id])}}",
                type: "POST",
                data: {
                    order_status: order_status,
                    _token: _token
                },
                success: function (response) {
                    if (response === 'load-pickup') {
                        $('#submit_status').val('تحویل بسته').attr("data-status", "load-receive")
                    }

                    if (response === 'load-receive') {
                        $('#submit_status').hide()
                        $('#cancel_button').hide()
                        $('.success').text('سفر با موفقیت پایان یافت .')
                    }
                },
                error: function (error) {
                    if (error) {
                        $('.error').text(error.responseJSON.message);
                    }
                }
            });
            console.log(result)
        });
    </script>
@endsection
