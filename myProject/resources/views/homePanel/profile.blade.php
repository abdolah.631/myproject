@extends('homePanel.homeLayouts.index')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                     src="{{asset('AdminPanelAssets')}}/dist/img/user4-128x128.jpg"
                                     alt="User profile picture">
                            </div>
                            <h3 class="profile-username text-center">{{$user->name}}</h3>
                            <p class="text-muted text-center">{{$user->email}}</p>
                        </div>
                    </div>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">درباره من</h3>
                        </div>
                        <div class="card-body row">
                            <p class="text-muted">
                                <span> <i class="fa fa-id-card mx-3"></i>کد ملی : </span>
                                {{$user->nation_code}}
                            </p>
                            <p class="text-muted">
                                <span> <i class="fa fa-person-burst mx-3"></i>جنسیت : </span>
                                {{$user->gender}}
                            </p>

                            <p class="text-muted ">
                                <span class="font-family-1 font-size-1"> <i class="fa fa-bore-hole mx-3 "></i>تاریخ تولد : </span>
                                {{$user->born_date}}
                            </p>

                            <p class="text-muted">
                                <span> <i class="fa fa-address-book mx-3"></i>آدرس : </span>
                                {{$user->address}}
                            </p>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#activity" data-toggle="tab">
                                        سفر ها
                                    </a>
                                </li>
                                @if(!is_null($user->biker))
                                    <li class="nav-item">
                                        <a class="nav-link" href="#cars" data-toggle="tab">
                                            خودرو ها
                                        </a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link" href="#settings" data-toggle="tab">
                                        تنظیمات
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="activity">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h3 class="card-title">جدول سفرها</h3>
                                                    <div class="card-tools">
                                                        <div class="input-group input-group-sm" style="width: 150px;">
                                                            <input type="text" name="table_search"
                                                                   class="form-control float-right" placeholder="جستجو">
                                                            <div class="input-group-append">
                                                                <button type="submit" class="btn btn-default">
                                                                    <i class="fa fa-search"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body table-responsive p-0">
                                                    <table class="table table-hover">
                                                        <tbody>
                                                        <tr>
                                                            <th>شماره</th>
                                                            <th>هزینه</th>
                                                            <th>توضیحات</th>
                                                            <th>ادرس گیرنده</th>
                                                            <th>ادرس فرستنده</th>
                                                            <th>وضعیت</th>
                                                        </tr>
                                                        @foreach($user->order as $order)
                                                            <tr>
                                                                <td>{{$order->id}}</td>
                                                                <td>{{$order->cost}}</td>
                                                                <td>{{$order->description}}</td>
                                                                <td>{{$order->addresses->recipient_address}}</td>
                                                                <td>{{$order->addresses->recipient_phone}}</td>
                                                                <td>
                                                                    @switch($order->status)
                                                                        @case('cancel-order')
                                                                            <span class="badge badge-danger">کنسل</span>
                                                                            @break
                                                                        @case('search-biker')
                                                                            <span class="badge badge-warning">جستجوی راننده</span>
                                                                            @break
                                                                        @case('biker-choices')
                                                                            <span
                                                                                class="badge badge-info">انتخاب شده</span>
                                                                            @break
                                                                        @case('load-pickup')
                                                                            <span
                                                                                class="badge badge-primary">دریافت</span>
                                                                            @break
                                                                        @case('load-receive')
                                                                            <span
                                                                                class="badge badge-success">تحویل</span>
                                                                            @break
                                                                    @endswitch
                                                                </td>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(!is_null($user->biker))
                                    <div class="tab-pane" id="cars">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h3 class="card-title">جدول خودرو ها</h3>
                                                        <div class="card-tools">
                                                            <div class="input-group input-group-sm"
                                                                 style="width: 150px;">
                                                                <input type="text" name="table_search"
                                                                       class="form-control float-right"
                                                                       placeholder="جستجو">
                                                                <div class="input-group-append">
                                                                    <button type="submit" class="btn btn-default">
                                                                        <i class="fa fa-search"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body table-responsive p-0">
                                                        <table class="table table-hover">
                                                            <tbody>
                                                            <tr>
                                                                <th>شماره</th>
                                                                <th>نام خودرو</th>
                                                                <th>شماره پلاک</th>
                                                                <th>وضعیت</th>
                                                                <th>سال ساخت</th>
                                                                <th>رنگ</th>
                                                                <th>ویرایش</th>
                                                            </tr>

                                                            @foreach($user->biker->vehicle as $vehicle)
                                                                <tr>
                                                                    <td>{{$vehicle->id}}</td>
                                                                    <td>{{$vehicle->name}}</td>
                                                                    <td>{{$vehicle->plaque_number}}</td>
                                                                    <td>{{$vehicle->is_active}}</td>
                                                                    <td>{{$vehicle->year_of_construction}}</td>
                                                                    <td>{{$vehicle->color}}</td>
                                                                    <td>operation</td>
                                                                </tr>
                                                            @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class=" tab-pane" id="settings">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="inputName" class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="inputName"
                                                       placeholder="Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="inputEmail"
                                                       placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputName2" class="col-sm-2 control-label">Name</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputName2"
                                                       placeholder="Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputExperience"
                                                   class="col-sm-2 control-label">Experience</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="inputExperience"
                                                          placeholder="Experience">

                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputSkills" class="col-sm-2 control-label">Skills</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputSkills"
                                                       placeholder="Skills">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"> I agree to the <a href="#">terms and
                                                            conditions</a>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-danger">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
