@extends('homePanel.homeLayouts.index')

@section('content')
    <section id="services" class="content m-auto">
        <div class="container-fluid">
            <div class="row">
                <h2 class="mb-1 text-center ">سفرهای درخواستی</h2>
                @include('AdminPanel.adminLayouts.errors')
                @foreach($orders as $order)
                    <div class="card text-black text-center  my-3 mx-2 " style="max-width: 19rem; background-color: #FFD34F;">
                        <span class="text-sm">شماره ({{$order->id }})</span>
                        <div class="card-header"> نام : {{$order->addresses->full_name }}</div>
                        <div class="card-body">
                            <h5 class="card-title"> ادرس فرستنده :</h5>
                            <div class="bg-warning rounded col-12">
                                <p class="card-text text-sm p-0">{{$order->addresses->sender_address}}</p>
                            </div>
                            <hr>
                            <h5 class="card-title"> ادرس گیرنده : </h5>
                            <div class="bg-warning rounded col-12">
                                <p class="card-text text-sm">{{$order->addresses->recipient_address }}</p>
                            </div>
                            <hr>
                            <h5 class="card-title"> تلفن گیرنده : </h5>
                            <div class="bg-warning rounded col-12">
                                <p class="card-text">{{$order->addresses->recipient_address }}</p>
                            </div>
                            <hr>
                            <h5 class="card-title">توضیحات : </h5>
                            <div class="bg-warning rounded col-12">
                                <p class="card-text">{{$order->description}}</p>
                            </div>
                        </div>
                        <a href="{{route('biker.order', ['biker' => $biker, 'order' => $order->id])}}" type="button" class="btn btn-info col-5 mb-2">
                            قبول سفر
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
{{--        {{$orders->links()}}--}}
    </section>
@endsection
