@extends('homePanel.homeLayouts.index')


@section('content')
    <div class="container-fluid" style="min-height: 400px">
        <section id="sevices" class="content">
            @include('AdminPanel.adminLayouts.errors')
            <div class="container-fluid">
                <div class=" card-body row   ">
                    @include('AdminPanel.adminLayouts.errors')
                    <h2 class="mb-2 text-center">سفرهای شما </h2>
                    @foreach($userOrders as $userOrder)
                        <div class="card card-default bg-warning-gradient ">
                            <div class="card-header">
                                <div class="d-flex justify-content-around">
                                    <p class="card-title text-white text-sm badge badge-info">شماره ({{$userOrder->id }}
                                        )</p>
                                    <div>
                                        @switch($userOrder->status)
                                            @case('search-biker')
                                                <span class="text-secondary">
                                                    در حال جستجوی راننده
                                                    <i class="fa-solid fa-magnifying-glass text-dark"></i>
                                                </span>
                                                @break
                                            @case('cancel-order')
                                                <span class="text-danger">
                                                    سفر شما لغو شده است
                                                    <i class="fa-solid fa-ban text-dark"></i>
                                                </span>
                                                @break
                                            @case('biker-choices')
                                                <span class="text-info">
                                                         راننده    {{$userOrder->biker[0]->biker->user->first_name}}      سفر شما را قبول کرد
                                                    <i class="fa-solid fa-person-biking text-dark"></i>
                                                </span>
                                                @break
                                            @case('load-pickup')
                                                <span class="text-primary">
                                                    راننده بسته را تحویل گرفت
                                                    <i class="fa-solid fa-person-biking-mountain text-dark"></i>
                                                </span>

                                                @break
                                            @case('load-receive')
                                                <span class="text-success">
                                                    سفر با موفقیت به پایان رسیده است
                                                </span>
                                                @break
                                        @endswitch
                                    </div>
                                </div>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse">
                                        <i class="fa fa-minus "></i>
                                    </button>
                                    <button type="button" class="btn btn-tool " data-widget="remove">
                                        <i class="fa fa-remove text-danger"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group d-flex ">
                                            <label> <i class="fa fa-person text-info"></i> نام کاربری : </label>
                                            <div class=" rounded col-5">
                                                <p class="card-text">{{$userOrder->addresses->full_name }}</p>
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label> <i class="fa fa-phone text-info"></i> تلفن کاربر : </label>
                                            <div class=" rounded col-5">
                                                <p class="card-text">{{$userOrder->addresses->sender_phone }}</p>
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label> <i class="fa fa-address-card text-info"></i> آدرس کاربر : </label>
                                            <div class=" rounded col-5">
                                                <p class="card-text">{{$userOrder->addresses->sender_address }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <div class="form-group d-flex">
                                                <label> <i class="fa-solid fa-money-bill-1 text-info"></i> هزینه :
                                                </label>
                                                <div class=" rounded col-5">
                                                    <p class="card-text">{{$userOrder->cost }}</p>
                                                </div>
                                            </div>
                                            <div class="form-group d-flex">
                                                <label> <i class="fa-solid fa-credit-card text-info"></i> وضعیت پرداخت :</label>
                                                <div class=" rounded col-5">
                                                    <p class="card-text">{{$userOrder->payment_status }}</p>
                                                </div>
                                            </div>
                                            <div class="form-group d-flex">
                                                <label> <i class="fa-solid fa-address-card text-info"></i> آدرس گیرنده :
                                                </label>
                                                <div class="rounded col-5">
                                                    <p class="card-text">{{$userOrder->addresses->recipient_address }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="form-group d-flex">
                                                <label> <i class="fa-solid fa-mobile-screen text-info"></i> تلفن گیرنده
                                                    :</label>
                                                <div class=" rounded col-5">
                                                    <p class="card-text">{{$userOrder->addresses->recipient_phone }}</p>
                                                </div>
                                            </div>
                                            <div class="form-group d-flex">
                                                <label> <i class="fa-solid fa-bars-progress text-info"></i> توضیحات
                                                    :</label>
                                                <div class=" rounded col-5">
                                                    <textarea class="card-text">{{$userOrder->description }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between col-md-12">
                                        <div class="rpw col-md-5">
                                            @if($userOrder->status !== 'load-receive'  || $userOrder->status !== 'cancel-order')
                                                <a href="{{route('user.cansel.service', ['order' => $userOrder->id])}}"
                                                   type="button"
                                                   id="cancel_button"
                                                   class="btn btn-danger col-3 mx-3 ">لغو سفر</a>
                                            @endif
                                            @if($userOrder->status !== 'search-biker' || $userOrder->status !== 'cancel-order' )
                                                @if($userOrder->payment_status == 'paid')
                                                    <a disabled
                                                       type="button"
                                                       id="cancel_button"
                                                       class="btn btn-info col-3 mx-3 ">پرداخت شده</a>
                                                @else
                                                    <a href="{{route('payment', ['order' => $userOrder->id])}}"
                                                       type="button"
                                                       id="cancel_button"
                                                       class="btn btn-success col-3 mx-3 ">پرداخت</a>
                                                @endif
                                            @endif
                                        </div>
                                        @if($userOrder->biker->isNotEmpty())
                                            <div class="row col-lg-6 ">
                                                <div class="col-lg-11  d-flex justify-content-end">
                                                    <div class="star-rating"
                                                         data-id="{{$userOrder->id}}">
                                                        <span class="fa  fa-star-o"
                                                              data-id="{{$userOrder->id}}"
                                                              data-rating="1"></span>
                                                        <span class="fa  fa-star-o"
                                                              data-id="{{$userOrder->id}}"
                                                              data-rating="2"></span>
                                                        <span class="fa  fa-star-o"
                                                              data-id="{{$userOrder->id}}"
                                                              data-rating="3"></span>
                                                        <span class="fa  fa-star-o"
                                                              data-id="{{$userOrder->id}}"
                                                              data-rating="4"></span>
                                                        <span class="fa fa-star-o"
                                                              data-id="{{$userOrder->id}}"
                                                              data-rating="5"></span>
                                                        <input type="hidden" name="rate-{{$userOrder->id}}"
                                                               id="rate-{{$userOrder->id}}"
                                                               class="rating-value"
                                                               value="{{$userOrder->rate ? $userOrder->rate->rate : '' }}">
                                                        <input type="hidden" name="biker-id-{{$userOrder->id}}"
                                                               id="biker-id-{{$userOrder->id}}"
                                                               class="rating-value"
                                                               value="{{$userOrder->biker->isNotEmpty() ? $userOrder->biker[0]->biker_id : ''}}">
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                </div>
                @endforeach
            </div>
        {{$userOrders->links()}}
    </div>
    </section>
    </div>
@endsection

@section('script')

    <script>
        let $star_rating = $('.star-rating');
        let SetRatingStar = function () {
            return $star_rating.each(function () {
                // console.log( $(this))
                $(this).children().each(function () {
                    // console.log($(this).siblings('input.rating-value').val())
                    if (parseInt($(this).siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                        return $(this).removeClass('fa-star-o').addClass('fa-star');
                    } else {
                        return $(this).removeClass('fa-star').addClass('fa-star-o');
                    }
                })
            })
        }

        let $star_rating_span = $('.star-rating .fa');

        $star_rating_span.on('click', function () {

            const rateId = $(this).data('id')
            const rateValue = $(this).data('rating')

            console.log($(`#rate-${rateId}`).val())
            $(`#rate-${rateId}`).val(rateValue)

            const result = $.ajax({
                url: "{{route('order.rate')}}",
                type: "POST",
                data: {
                    order_id: rateId,
                    biker_id: $(`#biker-id-${rateId}`).val(),
                    rate: rateValue,
                    _token: "{{csrf_token()}}"
                },
                success: function (response) {
                    if (response) {
                        console.log(response)
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error)
                        // $('.error').text(error.responseJSON.message);
                    }
                }
            });
            return SetRatingStar();
        });

        SetRatingStar();
        $(document).ready(function () {

        });
    </script>
    <style>
        body {
            background-color: #eee;
        }

        div.stars {
            width: 270px;
            display: inline-block;
        }

        .mt-200 {
            margin-top: 200px;
        }

        input.stars {
            display: none;
        }

        label.stars {
            float: right;
            padding: 5px;
            font-size: 20px;
            color: #4A148C;
            transition: all .2s;
        }

        input.stars:checked ~ label.stars:before {
            content: '\f005';
            color: #FD4;
            transition: all .25s;
        }

        input.star-5:checked ~ label.stars:before {
            color: #FE7;
            text-shadow: 0 0 20px #952;
        }

        input.star-1:checked ~ label.stars:before {
            color: #F62;
        }

        label.stars:hover {
            transform: rotate(-15deg) scale(1.3);
        }

        label.stars:before {
            content: '\f006';
            font-family: FontAwesome;
        }
    </style>
@endsection
