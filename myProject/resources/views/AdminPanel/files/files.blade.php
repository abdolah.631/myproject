@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>فایل ها</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">خانه</a></li>
                        <li class="breadcrumb-item active">فایل ها</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('content')
    <div class="card">
        <div class="row  justify-content-around">
            <div class="fa-2x mt-3 mr-2">
                <i class="fa-solid fa-picture-o text-danger "></i>
            </div>
            <div class=" mt-3">
                <div class="dataTables_length col-4 d-flex" id="example1_length">

                    <select name="example1_length" aria-controls="example1" class="form-control ">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <label class="mr-3">:Show</label>
                </div>
            </div>

            <div class="mt-3">
                <div id="example1_filter" class="dataTables_filter d-flex ">
                    <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example1">
                    <label class="mx-2">:Search</label>
                </div>
            </div>
        </div>
        <hr>
        <!-- /.card-header -->
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example2" class="table table-bordered table-hover dataTable text-center" role="grid">
                            <thead>
                            <tr role="row" class="bg-secondary-gradient small-box-footer">
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="موتور رندر:activat to sort column ascending">
                                    #User ID
                                </th>
                                <th class="sorting_desc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="سیستم عامل: activate to sort column ascending" aria-sort="descending">
                                    name
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="ورژن: activate to sort column ascending">
                                    extension
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    status
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    path (rootFile)
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    content_hash (rootFile)
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    mim_type (rootFile)
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    size (rootFile)
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($files as $file)
                                <tr role="row" class="odd">
                                    <td><a href="{{route('files.show' , ['file' => $file->id])}}"
                                           class="primary btn">{{ $file->id }}</a></td>
                                    <td>{{ $file->name }}</td>
                                    <td>{{ $file->extension }}</td>
                                    <td>{{ $file->status }}</td>
                                    <td class="sorting_1">{{ mb_substr($file->rootFile->path,0,10) }}</td>
                                    <td class="sorting_1">{{ mb_substr($file->rootFile->content_hash,0,10 )}}</td>
                                    <td class="sorting_1">{{ $file->rootFile->mim_type }}</td>
                                    <td class="sorting_1">{{ $file->rootFile->size }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm download" id="download"
                                           href="{{route('file.download' , ['file' =>$file->uuid])}}"  >
                                            <i class="fa fa-download"></i> download
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5"></div>
                    <div class="col-sm-12 col-md-7">
                        {{ $files->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

@endsection

@section('script')
    <script>

        // const download = document.getElementById('download')
        //
        // download.addEventListener('click', function (e) {
        //     event.preventDefault()
        //     console.log(1);
        // })
    </script>
@endsection
