@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>فایل</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('files.index')}}">فایلها</a></li>
                        <li class="breadcrumb-item active">مشاهده فایل</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <form action="" method="post" >
        @csrf
        @method('put')
        <div class="card-body  form-row m-2">
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">name</label>
                <input class="form-control" id="exampleInputEmail1" name="name"
                       placeholder="نام را وارد کنید" value="{{$file->name}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">email</label>
                <input type="email" class="form-control" id="exampleInputEmail1" name="email"
                       placeholder="ایمیل را وارد کنید" value="{{$file->email}}" disabled>
            </div>

        </div>

    </form>
@endsection
