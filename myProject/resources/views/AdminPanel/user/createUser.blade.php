@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>کاربر</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('users.index')}}">کاربران</a></li>
                        <li class="breadcrumb-item active">ایجاد کاربر</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <div class=" card  row m-4 bg-secondary">
        <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group" >
                    <label for="exampleInputEmail1">name</label>
                    <input class="form-control" id="exampleInputEmail1" name="name"
                           placeholder="نام را وارد کنید">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" name="email"
                           placeholder="ایمیل را وارد کنید">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password"
                           placeholder="پسورد را وارد کنید">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password_confirm</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password_confirmation"
                           placeholder="تکرار پسورد را وارد کنید">
                </div>


                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="remember">
                    <label class="form-check-label" for="exampleCheck1">مرا بخاطر بسپار</label>
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">ارسال</button>
            </div>
        </form>
    </div>
@endsection
