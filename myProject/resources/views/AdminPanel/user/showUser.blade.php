@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>کاربر</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('users.index')}}">کاربران</a></li>
                        <li class="breadcrumb-item active">مشاهده کاربر</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <form action="" method="post" >
        <div class="card-body  form-row m-2">
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">name</label>
                <input class="form-control" id="exampleInputEmail1" name="name"
                       value="{{$user->name}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">email</label>
                <input type="email" class="form-control" id="exampleInputEmail1" name="email"
                        value="{{$user->email}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">first_name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="first_name"
                      value="{{$user->first_name}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">last_name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="last_name"
                        value="{{$user->last_name}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">born_date</label>
                <input type="date" class="form-control" id="exampleInputEmail1" name="born_date"
                      value="{{$user->born_date}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">nation_code</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                      value="{{$user->nation_code}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">address</label>
                <textarea  class="form-control" id="exampleInputEmail1" name="address"
                           disabled> {{$user->address}}</textarea>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">gender</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="gender"
                        value="{{$user->gender}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">marriage_type</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="marriage_type"
                       value="{{$user->marriage_type}}" disabled>
            </div>
        </div>
    </form>
@endsection
