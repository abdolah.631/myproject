@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>کاربر</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('users.index')}}">کاربران</a></li>
                        <li class="breadcrumb-item active">ویرایش کاربر</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <form action="{{route('users.update', ['user'=>$user->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="card-body  form-row m-2 bg-info">
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">name</label>
                <input class="form-control" id="exampleInputEmail1" name="name"
                       placeholder="نام را وارد کنید" value="{{$user->name}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">email</label>
                <input type="email" class="form-control" id="exampleInputEmail1" name="email"
                       placeholder="ایمیل را وارد کنید" value="{{$user->email}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">first_name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="first_name"
                       placeholder="ایمیل را وارد کنید" value="{{$user->first_name}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">last_name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="last_name"
                       placeholder="ایمیل را وارد کنید" value="{{$user->last_name}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">born_date</label>
                <input type="date" class="form-control" id="exampleInputEmail1" name="born_date"
                       placeholder="ایمیل را وارد کنید" value="{{$user->born_date}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">nation_code</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                       placeholder="ایمیل را وارد کنید" value="{{$user->nation_code}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">address</label>
                <textarea type="text" class="form-control" id="exampleInputEmail1" name="address"
                          >{{$user->address}}</textarea>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">gender</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="gender"
                       placeholder="ایمیل را وارد کنید" value="{{$user->gender}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">marriage_type</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="marriage_type"
                       placeholder="ایمیل را وارد کنید" value="{{$user->marriage_type}}">
            </div>

            <div class="form-group">
                <label for="exampleInputFile">ارسال فایل</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="file">
                        <label class="custom-file-label" for="exampleInputFile">انتخاب فایل</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">ارسال</button>
        </div>
    </form>
@endsection
