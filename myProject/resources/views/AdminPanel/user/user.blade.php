@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>کاربران</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">خانه</a></li>
                        <li class="breadcrumb-item active">کاربران</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('content')
    <div class="card">
        <div class="row  justify-content-around">
            <div class="fa-2x mt-3 mr-2">
                <i class="fa-solid fa-person-burst "></i>
            </div>
            <div class=" mt-3">
                <div class="dataTables_length col-4 d-flex" id="example1_length">

                    <select name="example1_length" aria-controls="example1" class="form-control ">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <label class="mr-3">:Show</label>
                </div>
            </div>

            <div class="mt-3">
                <div id="example1_filter" class="dataTables_filter d-flex ">
                    <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example1">
                    <label class="mx-2">:Search</label>
                </div>
            </div>

            <div class=" d-inline-flex justify-content-around  ml-3 mt-3">
                <h3 class="card-title"></h3>
                <a href="{{route('users.create')}}">
                    <button type="button" class="btn btn-block btn-success ">
                        create user
                        <i class="fa fa-plus-square-o fa-1x"></i>
                    </button>
                </a>
            </div>
        </div>
        <hr>
        <!-- /.card-header -->
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example2" class="table table-bordered table-striped  table-hover dataTable" role="grid">
                            <thead>
                            <tr role="row" class="bg-secondary-gradient small-box-footer my-sm-auto">
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="موتور رندر:activat to sort column ascending">
                                    #User ID
                                </th>
                                <th class="sorting_desc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="سیستم عامل: activate to sort column ascending" aria-sort="descending">
                                    name
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="ورژن: activate to sort column ascending">
                                    email
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    is_verified
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr role="row" class=" small-box-footer my-auto">
                                    <td class=""><a href="{{route('users.show' , ['user' => $user->id])}}"
                                                    class="primary btn">{{ $user->id }} </a></td>
                                    <td>{{ $user->name }}</td>
                                    <td class="sorting_1">{{ $user->email }}</td>
                                    <td>{{ $user->email_verified_at }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                           href="{{route('users.edit' , ['user' =>$user->id])}}">
                                            <i class="fa fa-edit"></i> ویرایش
                                        </a>
                                        @if($user->isBiker == true)
                                            <i class="fa fa-bicycle"></i>
                                        @else
                                            <a class="btn  btn-sm {{ isset($user->bike) ? 'btn-light':'btn-success' }} "
                                               href="{{route('biker.create' , ['user' =>$user->id])}}">
                                                <i class="fa-solid fa-moped"></i>راننده
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5"></div>
                    <div class="col-sm-12 col-md-7">
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

@endsection
