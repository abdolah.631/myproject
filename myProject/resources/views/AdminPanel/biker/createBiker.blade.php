@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>راننده</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('bikers.index')}}">رانندگان</a></li>
                        <li class="breadcrumb-item active">ایجاد راننده</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')

    <form action="{{route('bikers.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="card col-lg-12">
                <div class="card-primary">
                    <div class="card-body row">
                        <div class="col-md-5 bg-secondary rounded">
                            <div class="form-group col-mx-5 my-2 mr-2">
                                <label for="exampleInputEmail1" class=" text-primary badge badge-warning">biker</label>
                            </div>

                            <div class="form-group">
                                @if($user->id)
                                    <label> username for create biker: <span
                                            class="badge badge-primary">{{$user->name}} </span> </label>
                                    <input type="hidden" value="{{$user->id}}" name="biker[user_id]" id="user_id">
                                @elseif(!$user->id)
                                    <label>راننده را انتخاب کنید</label>
                                    <select class="form-control" name="biker[user_id]" id="user_id">

                                        @foreach(\App\Models\User\User::all()  as $getUser)
                                            <option value="{{$getUser->id}}">{{$getUser->name}}</option>
                                        @endforeach()
                                    </select>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">is_active</label>
                                <select class="form-control" id="exampleInputPassword1" name="biker[is_active]">
                                    <option value="0">active</option>
                                    <option value="1">inactive</option>
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="exampleInputPassword1">status</label>
                                <select type="password" class="form-control" id="exampleInputPassword1"
                                        name="biker[status]">
                                    @foreach(\App\Models\Biker::STATUS as $status)
                                        <option value="{{$status}}">{{$status}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="exampleInputPassword1">تاریخ ثبت</label>
                                <input type="datetime-local" class="form-control" id="biker[registration_date_id]"
                                       name="registration_date">
                            </div>
                            <label for="exampleInputFile">ارسال فایل</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile"
                                           name="documents[identity_card]">
                                    <label class="custom-file-label" for="exampleInputFile">Identity Cart</label>
                                </div>
                            </div>

                            <div class="input-group mt-2">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile"
                                           name="documents[license]">
                                    <label class="custom-file-label" for="exampleInputFile">license Cart</label>
                                </div>
                            </div>

                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="col-md-6 bg-secondary rounded">

                            <div class="form-group col-mx-5 my-2 mr-">
                                <label for="" class="text-primary badge badge-warning">vehicle</label>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">vahicle name</label>
                                <input class="form-control" id="exampleInputEmail1" name="vehicle[name]">
                            </div>
                            <div class="form-group ">
                                <label for="exampleInputEmail1">vahicle_active </label>
                                <select class="form-control" id="exampleInputPassword1" name="vehicle[is_active]">
                                    <option value="0">active</option>
                                    <option value="1">inactive</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="exampleInputEmail1">color</label>
                                <input type="color" class="form-control" id="exampleInputEmail1" name="vehicle[color]">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">plaque_number</label>
                                <input type="text" class="form-control" id="exampleInputEmail1"
                                       name="vehicle[plaque_number]">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">year of construction</label>
                                <input type="number" class="form-control" id="exampleInputEmail1"
                                       name="vehicle[year_of_construction]">
                            </div>


                            <div class="form-group ">
                                <label for="exampleInputFile">ارسال فایل</label>

                                <div class="input-group mt-2">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile"
                                               name="documents[vehicle_card]">
                                        <label class="custom-file-label" for="exampleInputFile">vehicle Cart</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
