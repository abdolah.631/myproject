@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>خودرو ها</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">خانه</a></li>
                        <li class="breadcrumb-item active">خودرو ها</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('content')
    <div class="card">
        <div class="row  justify-content-around">
            <div class="fa-2x mt-3 mr-2">
                <i class="fa-solid fa-person-biking "></i>
            </div>
            <div class=" mt-3">
                <div class="dataTables_length col-4 d-flex" id="example1_length">

                    <select name="example1_length" aria-controls="example1" class="form-control ">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <label class="mr-3">:Show</label>
                </div>
            </div>

            <div class="mt-3">
                <div id="example1_filter" class="dataTables_filter d-flex ">
                    <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example1">
                    <label class="mx-2">:Search</label>
                </div>
            </div>

            @if(isset($biker))
            <div class=" d-inline-flex justify-content-around  ml-3 mt-3">
                <h3 class="card-title"></h3>
                <a href="{{route('vehicles.create', ['biker' => $biker])}}">
                    <button type="button" class="btn btn-block btn-success ">
                        create vehicle
                        <i class="fa fa-plus-square-o fa-1x"></i>
                    </button>
                </a>
            </div>
            @endif
        </div>
        <hr>

        <!-- /.card-header -->
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example2"
                               class="table table-bordered table-hover table-striped  text-md-center text-bold "
                               role="grid">
                            <thead>
                            <tr role="row" class=" bg-secondary-gradient">
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    style="width: 100.844px;"
                                    aria-label="موتور رندر:activat to sort column ascending">
                                    #Vehicle ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    style="width: 100.844px;"
                                    aria-label="موتور رندر:activat to sort column ascending">
                                    #Biker ID
                                </th>
{{--                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"--}}
{{--                                    aria-label="امتیاز: activate to sort column ascending">--}}
{{--                                    #Vehicle File--}}
{{--                                </th>--}}
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    name
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    is_active
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    plaque_number
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    year_of_construction
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    color
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vehicles as $vehicle)
                                <tr role="row" class="odd">
                                    <td class="sorting_1"><a
                                            href="{{route('vehicles.show' , ['vehicle' => $vehicle->id, 'biker' => $vehicle->biker_id])}}"
                                            class="primary btn">{{ $vehicle->id }}
                                    </td>
                                    <td class="sorting_1"><a
                                            href="{{route('bikers.show' , ['biker' => $vehicle->biker_id, 'biker' => $vehicle->biker_id])}}"
                                            class="primary btn">{{ $vehicle->biker_id }}
                                    </td>
{{--                                    <td class="sorting_1"><a--}}
{{--                                            href="{{route('files.show' , ['file' => $vehicle->vehicle_card_file_id])}}"--}}
{{--                                            class="primary btn">{{ $vehicle->vehicle_card_file_id }}--}}
{{--                                            <img src="{{}}" alt="">--}}
{{--                                    </td>--}}
                                    <td>{{ $vehicle->name }}</td>
                                    <td>{{ $vehicle->is_active }}</td>
                                    <td>{{ $vehicle->plaque_number }}</td>
                                    <td>{{ $vehicle->year_of_construction }}</td>
                                    <td>{{ $vehicle->color }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                           href="{{route('vehicles.edit' , ['vehicle' => $vehicle->id, 'biker' => $vehicle->biker_id])}}">
                                            <i class="fa fa-edit"></i> ویرایش
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5"></div>
                    <div class="col-sm-12 col-md-7">
                        {{ $vehicles->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>



@endsection
