@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>خودرو</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('vehicles.index', ['biker' => $vehicle->biker_id])}}">خودروها</a></li>
                        <li class="breadcrumb-item active">ویرایش خودرو</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <form
        action="{{route('vehicles.update', ['vehicle' => $vehicle->id, 'biker' => $vehicle->biker_id])}}"
          method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="card-body  form-row m-4 bg-info rounded">
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">username
                    : {{$vehicle->biker->user->name}}</label>
                <input class="form-control" id="exampleInputEmail1"
                       value="{{$vehicle->biker->user_id}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">Biker_id</label>
                <input class="form-control" id="exampleInputEmail1" name="biker_id"
                       value="{{$vehicle->biker_id}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">Vehicle Active</label>
                <select class="form-control" id="exampleInputEmail1" name="is_active">
                    <option value="0" {{$vehicle->is_active == 'inactive'? '' : 'selected'}}>inactive</option>
                    <option value="1" {{$vehicle->is_active == 'active' ? 'selected' : ''}}>active</option>
                </select>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">vehicle name <i class="fa fa-car"></i></label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="name"
                       value="{{$vehicle->name}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">plaque_number <i class="fa fa-list-numeric"></i></label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="plaque_number"
                       value="{{$vehicle->plaque_number}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">year_of_construction</label>
                <input type="number" class="form-control" id="exampleInputEmail1" name="year_of_construction"
                       value="{{$vehicle->year_of_construction}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">color</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="color"
                       value="{{$vehicle->color}}">
            </div>


            <div class="form-group">
                <label for="exampleInputFile">ارسال فایل</label>
                <div class="input-group mt-2">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="vehicle_card">
                        <label class="custom-file-label" for="exampleInputFile">vehicle_card</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">ارسال</button>
        </div>
    </form>
@endsection
