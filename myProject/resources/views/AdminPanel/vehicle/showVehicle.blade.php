@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>خودرو</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('vehicles.index')}}">خودرو ها</a></li>
                        <li class="breadcrumb-item active">مشاهده خودرو</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <form >
        <div class="card-body  form-row m-2">
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">name</label>
                <input class="form-control" id="exampleInputEmail1" name="name"
                       value="{{$vehicle->name}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">is_active</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="email"
                       value="{{$vehicle->is_active}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">plaque_number</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="first_name"
                       value="{{$vehicle->plaque_number}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">year_of_construction</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="last_name"
                       value="{{$vehicle->year_of_construction}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">vehicle_card_file_id</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="born_date"
                       value="{{$vehicle->vehicle_card_file_id}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">biker_id</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                       value="{{$vehicle->biker_id}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">color</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                       value="{{$vehicle->color}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">slug</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                       value="{{$vehicle->slug}}" disabled>
            </div>
        </div>
    </form>
@endsection
