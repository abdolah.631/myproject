@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>راننده</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('bikers.index')}}">رانندگان</a></li>
                        <li class="breadcrumb-item active">ویرایش راننده</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <form action="{{route('bikers.update', ['biker'=>$biker->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="card-body  form-row m-2 bg-info">
            <div class="align-content-center form-group col-md-12">
                <label for="exampleInputEmail1" class="badge-primary p-2 ">biker section</label>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">username
                    : {{\App\Models\User\User::query()->find($biker->user_id)->name}}</label>
                <input class="form-control" id="exampleInputEmail1" name="user_id"
                       value="{{$biker->user_id}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">biker_active</label>
                <select type="email" class="form-control" id="exampleInputEmail1" name="is_active">
                    @php
                        //                        dd( $biker->is_active)
                    @endphp
                    <option value="0" {{$biker->is_active == 'false'? '' : 'selected'}}>inactive</option>
                    <option value="1" {{$biker->is_active == 'true' ? 'selected' : ''}}>active</option>
                </select>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">registration_date</label>
                <input type="datetime-local" class="form-control" id="exampleInputEmail1" name="registration_date"
                       value="{{$biker->registration_date}}">
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">status</label>
                <select type="text" class="form-control" id="exampleInputEmail1" name="status"
                        value="{{$biker->status}}">
                    @foreach(\App\Models\Biker::STATUS as $status)
                        <option
                                value="{{$status}}" {{$biker->status == $status ? 'selected' : '' }}>{{$status}}</option>
                    @endforeach
                </select>
            </div>


            <div class="form-group col-md-5 row m-2">
                <label for="exampleInputFile">ارسال فایل</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile"
                               name="documents[identity_card]">
                        <label class="custom-file-label" for="exampleInputFile">identity_card</label>
                    </div>
                </div>

                <div class="input-group mt-2">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="documents[license]">
                        <label class="custom-file-label" for="exampleInputFile">license</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">ارسال</button>
        </div>
    </form>
    <hr>
    <div class="form-group col-mx-5 my-5 mr-5">
        <label for="exampleInputEmail1">vehicles of biker</label>
        @foreach($vehicles as $vehicle)
            <ul>
                <li><a href="{{route('vehicles.edit',['vehicle'=>$vehicle->id, 'biker' => $vehicle->biker_id])}}"><span class="text-success">vehicle name : </span> {{$vehicle->name}}</a></li>
            </ul>
        @endforeach
    </div>
@endsection
