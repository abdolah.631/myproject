@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>راننده</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">خانه</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('bikers.index')}}">رانندگان</a></li>
                        <li class="breadcrumb-item active">مشاهده راننده</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <form action="" method="">
        <div class="card-body  form-row m-2">
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">user_id</label>
                <input class="form-control" id="exampleInputEmail1" name="name"
                       value="{{$biker->user_id}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">is_active</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="email"
                       value="{{$biker->is_active}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">registration_date</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="first_name"
                       value="{{$biker->registration_date}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">identity_card_file_id</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="last_name"
                       value="{{$biker->identity_card_file_id}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">license_file_id</label>
                <input type="text" class="form-control" id="exampleInputEmail1"
                       value="{{$biker->license_file_id}}" disabled>
            </div>
            <div class="form-group col-md-5 m-2">
                <label for="exampleInputEmail1">status</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                       value="{{$biker->status}}" disabled>
            </div>
            @if($vehicles->toArray())
                <div class="align-content-center form-group mt-5 col-md-12">
                    <label for="exampleInputEmail1">vehicle section</label>
                </div>
            @endif

            <div class="row  ">
                @foreach($vehicles as $vehicle)
                    <div class="form-group col-md-5 m-2">
                        <label for="exampleInputEmail1">vehicle name</label>
                        <input type="text" class="form-control" id="exampleInputEmail1"
                               value="{{$vehicle->name}}" disabled>
                    </div>
                    <div class="form-group  m-2">
                        <label for="exampleInputEmail1">biker_id</label>
                        <input type="text" class="form-control" id="exampleInputEmail1"
                               value="{{$vehicle->biker_id}}" disabled>
                    </div>
                    <div class="form-group  m-2">
                        <label for="exampleInputEmail1">is_active</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                               value="{{$vehicle->is_active}}" disabled>
                    </div>
                    <div class="form-group  m-2">
                        <label for="exampleInputEmail1">slug</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                               value="{{$vehicle->slug}}" disabled>
                    </div>
                    <div class="form-group  m-2">
                        <label for="exampleInputEmail1">color</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                               value="{{$vehicle->color}}" disabled>
                    </div>
                    <div class="form-group  m-2">
                        <label for="exampleInputEmail1">vehicle_card_file_id</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                               value="{{$vehicle->vehicle_card_file_id}}" disabled>
                    </div>
                    <div class="form-group  m-2">
                        <label for="exampleInputEmail1">year_of_construction</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="nation_code"
                               value="{{$vehicle->year_of_construction}}" disabled>
                    </div>
<br>
                    <div class="align-content-center form-group mt-5 col-md-12">
                        <label for="exampleInputEmail1">--------------------------------------------</label>
                    </div>
                @endforeach
            </div>


        </div>
    </form>
@endsection
