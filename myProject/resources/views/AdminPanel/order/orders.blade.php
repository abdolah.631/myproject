@extends('AdminPanel.adminLayouts.master')

@section('contentHeader')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>راننده ها</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">خانه</a></li>
                        <li class="breadcrumb-item active">راننده ها</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('content')
    <div class="card">
        <div class="row  justify-content-around">
            <div class="fa-2x mt-3 mr-2">
                <i class="fa-solid fa-person-biking "></i>
            </div>
            <div class=" mt-3">
                <div class="dataTables_length col-4 d-flex" id="example1_length">

                    <select name="example1_length" aria-controls="example1" class="form-control ">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <label class="mr-3">:Show</label>
                </div>
            </div>

            <div class="mt-3">
                <div id="example1_filter" class="dataTables_filter d-flex ">
                    <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example1">
                    <label class="mx-2">:Search</label>
                </div>
            </div>

            <div class=" d-inline-flex justify-content-around  ml-3 mt-3">
                <h3 class="card-title"></h3>
                <a href="{{route('bikers.create')}}">
                    <button type="button" class="btn btn-block btn-success ">
                        create biker
                        <i class="fa fa-plus-square-o fa-1x"></i>
                    </button>
                </a>
            </div>
        </div>
        <hr>
        <!-- /.card-header -->
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example2"
                               class="table table-bordered table-striped table-hover dataTable text-md-center text-bold "
                               role="grid">
                            <thead>
                            <tr role="row" class="bg-secondary-gradient text-sm">
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="موتور رندر:activat to sort column ascending">
                                    #Biker ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="موتور رندر:activat to sort column ascending">
                                    #User ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="مرورگر: activate to sort column ascending">
                                    name <i class="fa fa-person"></i>
                                </th>
                                <th class="sorting_desc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="سیستم عامل: activate to sort column ascending" aria-sort="descending">
                                    Email <i class="fa-solid fa-envelope"></i>
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="ورژن: activate to sort column ascending">
                                    status <i class="fa-solid fa-bars"></i>
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    is_active <i class="fa-solid fa-check"></i>
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    vehicle count <i class="fa-solid fa-calculator"></i>
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                    aria-label="امتیاز: activate to sort column ascending">
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr role="row" class="odd">
                                    <td class=""><a href="{{route('bikers.show' , ['biker' => $order->id])}}"
                                                    class="primary btn">{{ $order->id }}  </td>
                                    <td class=""><a href="{{route('users.show' , ['user' => $order->user->id])}}"
                                                    class="primary btn">{{ $order->user->id }} </td>
                                    <td>{{ $order->user->name }}</td>
                                    <td class="sorting_1">{{ $order->user->email }}</td>
                                    <td>{{ $order->status }}</td>
                                    <td>{{ $order->is_active }}</td>
                                    <td>
                                        <a class="btn btn-sm btn-warning" href="{{ route('vehicles.index', ['biker' => $biker]) }}">
                                            <i class="fa fa-biking"></i> Bike List ({{ $order->vehicle()->count()}})
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                           href="{{route('bikers.edit' , ['biker' =>$order->id])}}">
                                            <i class="fa fa-edit"></i> ویرایش
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5"></div>
                    <div class="col-sm-12 col-md-7">
                        {{ $orders->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

@endsection
