@if ($errors && $errors->any())
    <div class="alert alert-danger text-center">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ( !empty($warning) )
    <div class="alert alert-warning alert-block text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong class="text-danger">{{ $warning }}</strong>
    </div>
@endif

@if ( !empty($success) )
    <div class="alert alert-warning alert-block text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong class="text-danger">{{ $success }}</strong>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block text-center ">
        <button type="button" class="close text-center" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


@if ($message = Session::get('info'))
    <div class="alert alert-info alert-block text-center ">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


{{--@if ($errors->any())--}}
{{--    <div class="alert alert-danger">--}}
{{--        <button type="button" class="close" data-dismiss="alert">×</button>--}}
{{--        Please check the form below for errors--}}
{{--    </div>--}}
{{--@endif--}}
