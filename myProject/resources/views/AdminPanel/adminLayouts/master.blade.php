<!DOCTYPE html>
<html>
<head>
    @include('AdminPanel.adminLayouts.head')
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    @include('AdminPanel.adminLayouts.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('AdminPanel.adminLayouts.aside')
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @yield('contentHeader')

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Default box -->
                @include('AdminPanel.adminLayouts.errors')
                @yield('content')
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('AdminPanel.adminLayouts.footer')
</div>
<!-- ./wrapper -->

@include('AdminPanel.adminLayouts.footerJs')
</body>
@yield('script')
</html>
