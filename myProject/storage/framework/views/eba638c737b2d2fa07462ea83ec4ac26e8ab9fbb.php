<!-- jQuery -->
<script src="<?php echo e(asset('AdminPanelAssets')); ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo e(asset('AdminPanelAssets')); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo e(asset('AdminPanelAssets')); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('AdminPanelAssets')); ?>/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('AdminPanelAssets')); ?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(asset('AdminPanelAssets')); ?>/dist/js/demo.js"></script>
<?php echo $__env->yieldContent('footerJs'); ?>
<?php /**PATH /home/app/myProject/resources/views/AdminPanel/adminLayouts/footerJs.blade.php ENDPATH**/ ?>