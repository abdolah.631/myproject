<!DOCTYPE html>
<html>
<head>
    <?php echo $__env->make('AdminPanel.adminLayouts.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <?php echo $__env->make('AdminPanel.adminLayouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php echo $__env->make('AdminPanel.adminLayouts.aside', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php echo $__env->yieldContent('contentHeader'); ?>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Default box -->
                <?php echo $__env->make('AdminPanel.adminLayouts.errors', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php echo $__env->yieldContent('content'); ?>
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php echo $__env->make('AdminPanel.adminLayouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<!-- ./wrapper -->

<?php echo $__env->make('AdminPanel.adminLayouts.footerJs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
<?php echo $__env->yieldContent('script'); ?>
</html>
<?php /**PATH /home/app/myProject/resources/views/AdminPanel/adminLayouts/master.blade.php ENDPATH**/ ?>