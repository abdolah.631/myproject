<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo e(route('dashboard')); ?>" class="brand-link">
        <img src="<?php echo e(asset('AdminPanelAssets')); ?>/dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">پنل مدیریت</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-user text-primary"></i>
                            <p>
                                <?php echo e(__('user.users')); ?>

                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo e(route('users.index')); ?>" class="nav-link">
                                    <i class="fa fa-users "></i>
                                    <p><?php echo e(__('user.users')); ?></p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo e(route('bikers.index')); ?>" class="nav-link">
                                    <i class="fa fa-motorcycle"></i>
                                    <p><?php echo e(__('user.bikers')); ?></p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-car text-danger"></i>
                            <p>
                                <?php echo e(__('vehicle.vehicle')); ?>

                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo e(route('vehicle.all-vehicles')); ?>" class="nav-link">
                                    <i class="fa fa-car "></i>
                                    <p><?php echo e(__('vehicle.vehicle')); ?></p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-file text-warning"></i>
                            <p>
                                <?php echo e(__('dashboard.files')); ?>

                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo e(route('files.index')); ?>" class="nav-link">
                                    <i class="fa fa-picture-o"></i>
                                    <p><?php echo e(__('dashboard.files')); ?></p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa-solid fa-bags-shopping text-warning"></i>
                            <p>
                               orders
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo e(route('orders.index')); ?>" class="nav-link">
                                    <i class="fa fa-picture-o"></i>
                                    <p>orders</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="<?php echo e(route('home')); ?>" class="nav-link btn btn-outline-info mt-5" target="_blank"> صفحه اصلی سایت</a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    </div>
    <!-- /.sidebar -->
</aside>
<?php /**PATH /home/app/myProject/resources/views/AdminPanel/adminLayouts/aside.blade.php ENDPATH**/ ?>