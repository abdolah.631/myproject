<?php if($errors && $errors->any()): ?>
    <div class="alert alert-danger text-center">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>

<?php if( !empty($warning) ): ?>
    <div class="alert alert-warning alert-block text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong class="text-danger"><?php echo e($warning); ?></strong>
    </div>
<?php endif; ?>

<?php if( !empty($success) ): ?>
    <div class="alert alert-warning alert-block text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong class="text-danger"><?php echo e($success); ?></strong>
    </div>
<?php endif; ?>

<?php if($message = Session::get('success')): ?>
    <div class="alert alert-success alert-block text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($message); ?></strong>
    </div>
<?php endif; ?>


<?php if($message = Session::get('error')): ?>
    <div class="alert alert-danger alert-block text-center ">
        <button type="button" class="close text-center" data-dismiss="alert">×</button>
        <strong><?php echo e($message); ?></strong>
    </div>
<?php endif; ?>


<?php if($message = Session::get('warning')): ?>
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($message); ?></strong>
    </div>
<?php endif; ?>


<?php if($message = Session::get('info')): ?>
    <div class="alert alert-info alert-block text-center ">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($message); ?></strong>
    </div>
<?php endif; ?>








<?php /**PATH /home/app/myProject/resources/views/AdminPanel/adminLayouts/errors.blade.php ENDPATH**/ ?>