<?php

return [
    'max_file_size' => 10240,
    'allowed_mime_types' => [
        'text/csv',
        'application/pdf',
        'image/svg+xml',

        //video
        'video/x-ms-asf',
        'video/x-flv',
        'video/mp4',
        'application/x-mpegURL',
        'video/MP2T',
        'video/3gpp',
        'video/quicktime',
        'video/x-msvideo',
        'video/x-ms-wmv',
        'video/avi',

        //image
        'image/jpg',
        'image/jpeg',
        'image/png',
        'image/x-icon',
        'image/vnd.microsoft.icon'
    ],
];
