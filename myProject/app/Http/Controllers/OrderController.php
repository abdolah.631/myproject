<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderAddressRequest;
use App\Models\Order\Order;
use App\Models\Order\OrderBiker;
use App\Models\Order\OrderRate;
use App\Repositories\OrderAddressRepository;
use App\Repositories\OrderRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    public OrderRepository $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $orders = Order::query()->paginate(10);

        return view('AdminPanel.order.orders', compact('orders'));
    }


    /**
     * @param OrderAddressRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeOrder(OrderAddressRequest $request)
    {
        try {
            DB::beginTransaction();

            $data = $request->validated();
            $data['user_id'] = Auth::user()->id ?? null;
            $data['cost'] = 200000;
            $data['status'] = Order::STATUS_SEARCH_BIKER;
            $data['payment_status'] = Order::PAYMENT_UNPAID;

            $order = $this->repository->store($data);

            $data['order_id'] = $order->id;

            $orderAddress = resolve(OrderAddressRepository::class)->store($data);

            DB::commit();

            return response()->json(['order' => $order, 'orderAddress' => $orderAddress], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $message
     *
     * @return Application|Factory|View
     */
    public function getServices($message = null)
    {

        $allOrders = Order::query()->with('addresses')
            ->with('biker', function ($q) {
                $q->where('status', 'accept');
            })->get();
        $orders = new  Collection();
        $biker = null;

        foreach ($allOrders as $order) {

            if ($order->status == 'load-receive') {
                continue;
            }
            $orders[] = $order;
        }

        if (Auth::user() && Auth::user()->isBiker) {
            $biker = Auth::user()->biker;

            if ($message) {
                return view('homePanel.services', compact('orders', 'biker'))
                    ->with(['warning' => $message]);
            }

            return view('homePanel.services', compact('orders', 'biker'));
        }

        return view('homePanel.home');
    }

    /**
     * @param Request $request
     * @param Order   $order
     *
     * @return array|string|string[]
     * @throws \Throwable
     */
    public function changeOrderStatus(Request $request, Order $order)
    {
        DB::beginTransaction();
        $order->update([
            'status' => $request->order_status,
        ]);

        if ($request->order_status == 'load-receive') {
            $order->biker()->update(['status' => OrderBiker::STATUS_COMPLETION]);
        }
        DB::commit();

        return $order->status;
    }

    public function orderRate(Request $request)
    {
        OrderRate::updateOrCreate([
            'order_id' => $request->order_id
        ],[
            'order_id' => $request->order_id ,
            'biker_id' => $request->biker_id,
            'rate' => $request->rate,
        ]);
    }

}
