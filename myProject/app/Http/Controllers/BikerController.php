<?php

namespace App\Http\Controllers;

use App\Constants\Permissions;
use App\Events\requestBiker;
use App\Http\Requests\BikerRequest;
use App\Http\Requests\BikerVehicleRequest;
use App\Models\Biker;
use App\Models\Order\Order;
use App\Models\Order\OrderBiker;
use App\Models\User\Permission;
use App\Models\User\Role;
use App\Models\User\User;
use App\Repositories\BikerRepository;
use App\Repositories\FileRepository;
use App\Repositories\vehicleRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\isEmpty;

class BikerController extends Controller
{
    public BikerRepository $repository;

    /**
     * @param BikerRepository $repository
     */
    public function __construct(BikerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $bikers = Biker::query()
            ->whereHas('user')
            ->with('user')
            ->simplePaginate(10);

        return view('AdminPanel.biker.bikers', compact('bikers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(User $user)
    {
        return view('AdminPanel.biker.createBiker', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BikerVehicleRequest $request, FileRepository $fileRepository, BikerRepository $bikerRepository, vehicleRepository $vehicleRepository)
    {
//        dd($request->toArray());
        DB::beginTransaction();
        $path = Biker::BIKER_DOCUMENT_PATH;
        try {
            $validatedData = $request->validated();
            $biker = $validatedData['biker'];
            $vehicle = $validatedData['vehicle'];

            $vehicle['vehicle_card_file_id'] = $biker['identity_card_file_id'] = $biker['license_file_id'] = null;
            if ($request->hasFile('documents.license')) {
                $biker['license_file_id'] = $fileRepository->upload($request->file('documents.license'), $path)->id;
            }
            if ($request->hasFile('documents.identity_card')) {
                $biker['identity_card_file_id'] = $fileRepository->upload($request->file('documents.identity_card'), $path)->id;
            }
            if ($request->hasFile('documents.vehicle_card')) {
                $vehicle['vehicle_card_file_id'] = $fileRepository->upload($request->file('documents.vehicle_card'), $path)->id;
            }

            $biker = $bikerRepository->store($biker);
            $permission = Permission::query()
                ->whereIn('name', [Permissions::CREATE_BIKER, Permissions::STORE_NEW_BIKER_ORDER])
                ->pluck('id');

            $bikerRole = Role::query()->where('name', Role::BIKER)->pluck('id');
            $biker->user->roles()->attach($bikerRole);


            $vehicle['biker_id'] = $biker->id;
            $vehicleRepository->store($vehicle);

            DB::commit();

            return redirect()->route('users.index')->with(['success' => 'create bike successfully']);
        } catch (\Exception $exception) {
            DB::rollBack();
            return back()->with(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Biker $biker
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Biker $biker)
    {
        $vehicles = $biker->vehicle()->get();

        return view('AdminPanel.biker.showBiker', compact('biker', 'vehicles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Biker $biker
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Biker $biker)
    {
        $vehicles = $biker->vehicle()->get();

        return view('AdminPanel.biker.editBiker', compact('biker', 'vehicles'));
    }

    /**
     * @param BikerRequest   $request
     * @param Biker          $biker
     * @param FileRepository $fileRepository
     *
     * @return RedirectResponse
     * @throws \Throwable
     */
    public function update(BikerRequest $request, Biker $biker, FileRepository $fileRepository)
    {
        DB::beginTransaction();
        $data = $request->validated();
        $path = Biker::BIKER_DOCUMENT_PATH;
        try {
            if ($request->hasFile('documents.identity_card')) {
                $data['identity_card_file_id'] = $fileRepository->upload($request->file('documents.identity_card'), $path)->id;
            }

            if ($request->hasFile('documents.license')) {
                $data['license_file_id'] = $fileRepository->upload($request->file('documents.license'), $path)->id;
            }

            $this->repository->update($data, $biker);
            DB::commit();

            return back()->with(['success' => 'updated successfully']);
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->with(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Biker $biker
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Biker $biker)
    {
        //
    }

    /**
     * @return string
     */
    public function searchBiker(Request $request)
    {
        event(new requestBiker($request->message));
    }


    /**
     * @throws \Throwable
     */
    public function storeBikerOrder(Biker $biker, Order $order)
    {
        $bikerHasOrder = $biker->isBikerAcceptingService()->first();

        if ($bikerHasOrder) {
            return view('homePanel.acceptedService',
                ['order' => $bikerHasOrder->order, 'warning' => 'شما سفر در حال اجرا دارید']
            );
        }

        DB::beginTransaction();
        OrderBiker::query()->Create(
            [
                'order_id' => $order->id,
                'biker_id' => $biker->id,
                'status' => OrderBiker::STATUS_ACCEPT,
            ]
        );

        $order->update(['status' => Order::STATUS_BIKER_CHOSEN]);
        DB::commit();
        return view('homePanel.acceptedService', compact('order'));
    }

    /**
     * @return Application|Factory|View
     */
    public function showAcceptedService()
    {
        $orders = Auth::user()->bikerOrder
            ->where('status', OrderBiker::STATUS_ACCEPT)
            ->load('order.addresses')->first();
//        $order = Auth::user()->biker->acceptedOrder();
        if ($orders) {
            $order = $orders->order;
//dd($order);
            if (!is_null($order) && $order->status == 'cancel-order') {
                return resolve(OrderController::class)->getServices('سرویس مورد نظر از طرف مشتری لغو شده است');
            }

            return view('homePanel.acceptedService', compact('order'));

        }
        return resolve(OrderController::class)->getServices('هنوز سرویسی انتخاب نکرده اید');

    }

    /**
     * @param Order $order
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function cancelService(Order $order)
    {
        $hasAcceptOrder = $order->biker->where('status', OrderBiker::STATUS_ACCEPT)->first();

        if ($hasAcceptOrder) {
            $hasAcceptOrder->update(['status' => OrderBiker::STATUS_CANCEL]);

            $order->update([
                'status' => Order::STATUS_SEARCH_BIKER,
            ]);
            return redirect('services')->with(['success' => 'سفر درخواستی ' . ' ' . $hasAcceptOrder->id . ' ' . 'با موفقیت لغو شد']);
        }

        return redirect('services')->with(['warning' => 'سفر درخواستی قبلا لغو شده بود']);
    }
}
