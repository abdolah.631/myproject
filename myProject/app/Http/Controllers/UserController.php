<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Order\Order;
use App\Models\Order\OrderBiker;
use App\Models\User\User;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public UserRepository $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $users = User::query()->with('biker')->paginate(10);

        return view('AdminPanel.user.user', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('AdminPanel.user.createUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(UserRequest $request)
    {
        $user = $request->validated();

        try {
            $this->repository->store($user);

            return back()->with('success', 'create successfully');
        } catch (\Exception $exception) {
            return back()->withInput()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(User $user)
    {
        return view('AdminPanel.user.showUser', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(User $user)
    {
        return view('AdminPanel.user.editUser', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User $user)
    {
        $data = $request->validated();

        try {
            $this->repository->update($data, $user);

            return back()->with('success', 'update successfully');
        } catch (\Exception $exception) {
            return back()->withInput()->withErrors($exception->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function profile()
    {
        $user = Auth::user()->load(['order.addresses', 'biker.vehicle']);
//        dd($user->toArray());

        return view('homePanel.profile', compact('user'));
    }


    public function userOrder($message = null)
    {
        if (Auth::user()) {
            $userOrders = Auth::user()
                ->order()
                ->with(['addresses', 'rate'])
                ->with('biker', function ($q) {
                    $q->whereIn('status', [OrderBiker::STATUS_ACCEPT, OrderBiker::STATUS_COMPLETION])->with('biker.user');
                })->paginate(10);
        }

        if ($userOrders) {
            if ($message) {
                return view('homePanel.userOrder', compact('userOrders'))->with('success', $message);
            }

            return view('homePanel.userOrder', compact('userOrders'));
        }

        return view('homePanel.home');
    }


    public function userCanselService(Order $order)
    {
        $order->update([
            'status' => Order::STATUS_CANCEL_ORDER,
        ]);
    }
}
