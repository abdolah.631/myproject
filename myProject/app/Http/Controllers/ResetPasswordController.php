<?php

namespace App\Http\Controllers;

use App\Events\PasswordResetEvent;
use App\Http\Requests\ResetPasswordRequest;
use App\Models\User\ResetPassword;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{

    /**
     * @param ResetPasswordRequest $request
     *
     * @return RedirectResponse
     */
    public function resetPasswordEmail(ResetPasswordRequest $request)
    {
        $resetPassword = ResetPassword::query()->where('email', $request->validated()['email'])->first();

        $expiredAt = optional($resetPassword)->getExpiredAt();

        if (!empty($expiredAt) && now()->lt($expiredAt)) {
            return back()->with(['error' => 'too many request']);
        }

        $resetPassword = $this->upsertPasswordResetRecord($request);

        event(new passwordResetEvent($resetPassword));

        return back()->with(['success' => 'reset password link send to your email']);
    }

    /**
     * @param Request $request
     *
     * @return Builder|Model
     */
    public function upsertPasswordResetRecord(Request $request)
    {
        return ResetPassword::query()->updateOrCreate(
            [
                'email' => $request->email,
            ],
            [
                'email' => $request->email,
                'token' => $this->createToken($request->validate()['email']),
                'expired_at' => Carbon::now()->addMinutes(ResetPassword::INTERVAL_RESEND),
                'created_at' => Carbon::now(),
            ]
        );
    }

    public function createToken($email)
    {
        return md5(Str::random(64) . microtime() . $email);
    }

}
