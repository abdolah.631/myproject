<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileRequest;
use App\Models\File\File;
use App\Repositories\FileRepository;
use Illuminate\Http\Request;

class FileController extends Controller
{

    private FileRepository $repository;

    /**
     * @param FileRepository $repository
     */
    public function __construct(FileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $files = File::query()->with('rootFile')->paginate(10);

        return view('AdminPanel.files.files', compact('files'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return File
     * @throws \Exception
     */
    public function store(FileRequest $request)
    {
        try {
            $file = $request->validated();

            $file = $this->repository->upload(
                $file->getRealPath(),
                $file->getClientOriginalName(),
                $file->extension(),
                $file->getSize(),
                $file->getMimeType(),
            );
        } catch (\Exception $exception) {
            throw new $exception;
        }

        return $file;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\File\File $files
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(File $file)
    {
        return view('AdminPanel.files.showFile', compact('file'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\File\File $files
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(File $files)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\File\File    $files
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $files)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\File\File $files
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $files)
    {
        //
    }


    /**
     * @param File $file
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function download(file $file)
    {
        try {
            return Response()->download(
                \Storage::disk(config('filesystems.default'))->path($file->rootFile->path),
                $file->name,
                [
                    'content-Type:' . $file->rootFile->mime_type,
                    'content-length:' . $file->size,
                ]
            );


        }catch (\Exception $exception){
            return  back()->with(['error'=> 'the file not exist']);
        }

    }
}
