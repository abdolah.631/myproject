<?php

namespace App\Http\Controllers;

use App\Http\Requests\VehicleRequest;
use App\Models\Biker;
use App\Models\Vehicle;
use App\Repositories\FileRepository;
use App\Repositories\vehicleRepository;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    private vehicleRepository $repository;

    /**
     * @param vehicleRepository $repository
     */
    public function __construct(vehicleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function allVehicles()
    {
        $vehicles = Vehicle::query()->paginate(10);

        return view('AdminPanel.vehicle.vehicle', compact('vehicles'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Biker $biker)
    {
        $vehicles = $biker->vehicle()->paginate();

        return view('AdminPanel.vehicle.vehicle', compact('vehicles', 'biker'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Vehicle $vehicle
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Vehicle $vehicle, Biker $biker)
    {
        return view('AdminPanel.vehicle.showVehicle', compact('vehicle', 'biker'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Vehicle $vehicle
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Biker $biker, Vehicle $vehicle)
    {
        return view('AdminPanel.vehicle.editVehicle', compact('vehicle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Vehicle      $vehicle
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VehicleRequest $request, Biker $biker, Vehicle $vehicle, FileRepository $fileRepository)
    {
        try {
            $data = $request->validated();
            $path = '/biker_document';
            if ($request->hasFile('vehicle_card')) {
                $data['vehicle_card_file_id'] = $fileRepository->upload($request->file('vehicle_card'), $path)->id;
            }

            $this->repository->update($data, $vehicle);

            return back()->with(['success' => 'update successfully']);
        } catch (\Exception $exception) {
            return back()->with(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Vehicle $vehicle
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        //
    }
}
