<?php

namespace App\Http\Controllers;

use App\Models\Order\Order;
use App\Models\Payment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Throwable;

class PaymentController extends Controller
{
    /**
     * @param Order $order
     *
     * @return RedirectResponse
     */
    public function payment(Order $order)
    {

        try {
            $payment = Payment::query()->create([
                'order_id' => $order->id,
                'payment_status' => Payment::PAYMENT_STATUS_UNPAID,
                'amount' => $order->cost,
            ]);
        } catch (\Exception $exception) {
            return back()->with('info', 'error payment create :' . $exception);
        }


        $Amount = $order->cost;// $order->cost; //Amount will be based on Toman - Required
        $url = url('payment/verify');
        $data = [
            'MerchantID' => '0f9d808e-d7ec-11e7-8b16-005056a205be',
            'Amount' => $Amount,
            'CallbackURL' => $url,
            'Description' => 'خرید تست',
        ];

        $jsonData = json_encode($data);
        $ch = curl_init('https://sandbox.zarinpal.com/pg/rest/WebGate/PaymentRequest.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData),
        ]);

        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        if ($err) {
            return back()->with('error', 'error payment:' . $err);
        } else {

            if ($result["Status"] == 100) {
                $payment->update([
                    'Authority' => $result["Authority"],
                ]);
                header('Content-Type: application/json');
                $go = 'https://sandbox.zarinpal.com/pg/StartPay/' . $result['Authority'];
                return redirect()->to($go);
            } else {
                return back()->with('error', 'error payment page:' . $result["Status"]);
            }
        }
    }

    /**
     * @throws Throwable
     */
    public function paymentVerify(Request $request)
    {
        $MerchantID = '0f9d808e-d7ec-11e7-8b16-005056a205be';
        $payment = Payment::query()->where('Authority', $request->Authority)->first();

        $Authority = $request->Authority;

        $data = ['MerchantID' => $MerchantID, 'Authority' => $Authority, 'Amount' => $payment->amount];
        $jsonData = json_encode($data);
        $ch = curl_init('https://sandbox.zarinpal.com/pg/rest/WebGate/PaymentVerification.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData),
        ]);
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result, true);

        if ($err) {
            resolve(UserController::class)->userOrder('error:' . $err);
        } else {
            if ($result['Status'] == 100) {
                try {
                    DB::beginTransaction();
                    $payment->update([
                        'ref_id' => $result['RefID'],
                        'payment_status' => Payment::PAYMENT_STATUS_PAID,
                    ]);

                    $payment->order->update([
                        'payment_status' => Order::PAYMENT_PAiD,
                    ]);

                    DB::commit();

                    return resolve(UserController::class)->userOrder('با موفقیت پرداخت شد.');
                } catch (\Exception $exception) {
                    DB::rollBack();

                    return resolve(UserController::class)->userOrder('error:' . $exception);
                }
            } else if ($result['Status'] == 101) {

                return resolve(UserController::class)->userOrder('با موفقیت پرداخت شده است verify.');
            } else {

                return resolve(UserController::class)->userOrder('Transaction failed. Status:' . $result['Status']);
            }
        }
    }

}
