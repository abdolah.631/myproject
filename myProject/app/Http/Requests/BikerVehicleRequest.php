<?php

namespace App\Http\Requests;

use App\Models\Biker;
use App\Models\User\User;
use App\Rules\UseNotBeBikerRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BikerVehicleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        //['biker' => [...], 'vehicle' => [...], 'documents'=> [...]]
        return $this->vehicleRules() + $this->bikerRules() + $this->documentRules();
    }

    public function bikerRules(): array
    {
        return [
            'biker' => ['array'],
            'biker.user_id' => ['required', 'integer', Rule::exists(User::TABLE, 'id'), new UseNotBeBikerRule()],
            'biker.is_active' => ['required', 'boolean'],
            'biker.registration_date' => ['nullable', 'date'],
            'biker.status' => ['nullable', 'string', Rule::in(Biker::STATUS)],
        ];
    }

    public function documentRules(): array
    {
        return [
            'documents' => ['nullable', 'array'],
            'documents.identity_card' => ['nullable', 'file'],//make them as more validated
            'documents.license' => ['nullable', 'file'],//make them as more validated
            'documents.vehicle_card' => ['nullable', 'file'],//make them as more validated
        ];
    }

    public function vehicleRules(): array
    {
        return [
            'vehicle' => ['array'],
            'vehicle.name' => ['required', 'string'],
            'vehicle.is_active' => ['required', 'string'],
            'vehicle.color' => ['nullable', 'string'],
            'vehicle.year_of_construction' => ['nullable', 'string'],
            'vehicle.plaque_number' => ['nullable', 'string'],
        ];
    }
}
