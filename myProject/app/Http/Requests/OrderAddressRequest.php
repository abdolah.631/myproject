<?php

namespace App\Http\Requests;

use App\Models\Order\Order;
use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderAddressRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [

            // order
//            'user_id' => ['nullable', 'integer', Rule::in(User::TABLE)],
            'description' => ['nullable', 'string'],


            //addresses
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'sender_phone' => ['required', 'integer'],
            'recipient_phone' => ['required', 'integer'],
            'sender_address' => ['required', 'string'],
            'recipient_address' => ['required', 'string'],



        ];
    }
}
