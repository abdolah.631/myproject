<?php

namespace App\Http\Requests;

use App\Models\Biker;
use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BikerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_id' => ['required', 'integer', Rule::exists(User::TABLE, 'id')],
            'is_active' => ['required', 'boolean'],
            'registration_date' => ['nullable', 'date'],
            'status' => ['nullable', 'string', Rule::in(Biker::STATUS)],
            'documents'=> ['array'],
            'documents.identity_card' => ['nullable', 'file'],//make them as more validated
            'documents.license' => ['nullable', 'file'],
        ];
    }

    public function vehicleRules(): array
    {
        return [
            'vehicle' => ['array'],
            'vehicle.name' => ['required', 'string'],
            'vehicle.is_active' => ['required', 'string'],
            'vehicle.color' => ['nullable', 'string'],
            'vehicle.year_of_construction' => ['nullable', 'string'],
            'vehicle.plaque_number' => ['nullable', 'string'],
        ];
    }
}
