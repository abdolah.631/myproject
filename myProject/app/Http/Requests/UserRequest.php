<?php

namespace App\Http\Requests;

use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users' , 'email')->ignore($this->user?->id) ],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
            'born_date' => ['nullable', 'date'],
            'nation_code' => ['nullable', 'integer'],
            'address' => ['nullable', 'string'],
            'gender' => ['nullable', 'string', Rule::in(User::GENDER)],
            'marriage_type' => ['nullable', 'string', Rule::in(User::MARRIAGE_TYPE)],
        ];
    }
}
