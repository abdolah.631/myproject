<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'file' => [
                'required',
                'file',
                'max:' . config('cdn.max_file_size'),
                'mimetypes:' . config('cdn.allowed_mime_types'),
            ],
        ];
    }
}
