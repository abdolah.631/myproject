<?php

namespace App\Http\Requests;

use App\Models\Biker;
use App\Rules\BikerJustAndOnlyJustHaveOneActiveVehicle;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $biker = $this->route('biker');
        return [
            'name' => ['required', 'string'],
            'is_active' => ['required', 'boolean', new BikerJustAndOnlyJustHaveOneActiveVehicle($biker)],
            'color' => ['nullable', 'string'],
            'year_of_construction' => ['nullable', 'integer'],
            'vehicle_card' => ['nullable', 'file'],
            'plaque_number' => ['nullable', 'integer'],
        ];
    }
}
