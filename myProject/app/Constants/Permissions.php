<?php

namespace App\Constants;

class Permissions
{
    const MANAGE_USER = 'manageUser';
    const MANAGE_BIKER = 'manageBiker';
    const MANAGE_VEHICLE = 'manageVehicle';
    const MANAGE_ORDERS = 'manageOrders';
    const MANAGE_FILES = 'manageFiles';
    const CREATE_BIKER = 'createBiker';
    const STORE_NEW_BIKER_ORDER = 'storeNewBikerOrder';
    const SHOW_ACCEPTED_SERVICE = 'showAcceptedService';
    const CANCEL_SERVICE = 'cancelService';
}
