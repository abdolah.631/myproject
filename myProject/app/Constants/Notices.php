<?php

namespace App\Constants;

class Notices
{
    public const USER_RESET_PASSWORD = 'user_reset_password';
    public const USER_REGISTRATION = 'user_registration';
}
