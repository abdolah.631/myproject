<?php

namespace App\Constants\RolePermissions;

use App\Constants\Permissions;

class AdminPermissions
{
    const PERMISSIONS = [
        Permissions::MANAGE_USER,
        Permissions::MANAGE_BIKER,
        Permissions::MANAGE_VEHICLE,
        Permissions::MANAGE_ORDERS,
        Permissions::MANAGE_FILES,
        Permissions::CREATE_BIKER,
        Permissions::STORE_NEW_BIKER_ORDER,
        Permissions::SHOW_ACCEPTED_SERVICE,
        Permissions::CANCEL_SERVICE,
    ];
}
