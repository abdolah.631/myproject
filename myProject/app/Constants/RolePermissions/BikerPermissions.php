<?php

namespace App\Constants\RolePermissions;

use App\Constants\Permissions;

class BikerPermissions
{
    const PERMISSIONS = [
        Permissions::STORE_NEW_BIKER_ORDER,
        Permissions::SHOW_ACCEPTED_SERVICE,
        Permissions::CANCEL_SERVICE,
    ];
}
