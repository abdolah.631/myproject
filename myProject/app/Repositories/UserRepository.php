<?php
namespace App\Repositories;
use App\Models\User\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{

    public function store(array $data)
    {
        return User::firstOrCreate([
            'name' => $data['name'],
            'first_name' => $data['name'],
            'last_name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'born_date' => $data['born_date'] ?? null,
            'nation_code' => $data['nation_code'] ?? null,
            'address' => $data['address'] ?? null,
            'gender' => $data['gender'] ?? null,
            'marriage_type' => $data['marriage_type'] ?? null,
        ]);
    }

    public function update(array $data ,User $user)
    {
        $user->update($data);
    }

}
