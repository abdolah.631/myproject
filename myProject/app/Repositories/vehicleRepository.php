<?php

namespace App\Repositories;

use App\Models\Biker;
use App\Models\Vehicle;
use Carbon\Carbon;

class vehicleRepository
{

    /**
     * store new biker.
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store(array $data)
    {
        return Vehicle::query()->create([
            'name' => $data['name'],
            'is_active' => $data['is_active'],
            'color' => $data['color'],
            'plaque_number' => $data['plaque_number'],
            'vehicle_card_file_id' => $data['vehicle_card_file_id'],
            'year_of_construction' => $data['year_of_construction'],
            'biker_id' => $data['biker_id'],
        ]);
    }

    public function update(array $data, Vehicle $vehicle)
    {
        $vehicle->update($data);
    }

}
