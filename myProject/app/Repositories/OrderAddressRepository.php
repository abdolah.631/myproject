<?php

namespace App\Repositories;

use App\Models\Biker;
use App\Models\Order\OrderAddress;
use Carbon\Carbon;

class OrderAddressRepository
{

    /**
     * store new OrderAddress.
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store(array $data)
    {

        return OrderAddress::query()->create([
            'order_id' => $data['order_id'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'sender_phone' => $data['sender_phone'],
            'recipient_phone' => $data['recipient_phone'],
            'sender_address' => $data['sender_address'],
            'recipient_address' => $data['recipient_address'],
//            'begin_latitude' => $data['begin_latitude'] ?? null,
//            'begin_longitude' => $data['begin_longitude'] ?? null,
//            'destination_latitude' => $data['destination_latitude'] ?? null,
//            'destination_longitude' => $data['destination_longitude'] ?? null,
        ]);
    }

    public function update(array $data, OrderAddress $orderAddress)
    {
        $orderAddress->update($data);
    }

}
