<?php

namespace App\Repositories;

use App\Models\Biker;
use Carbon\Carbon;

class BikerRepository
{

    /**
     * store new biker.
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store(array $data)
    {
        return Biker::query()->create([
            'user_id' => $data['user_id'],
            'is_active' => $data['is_active'],
            'status' => $data['status'],
            'registration_date' => $data['registration_date'] ?? Carbon::now(),
            'identity_card_file_id' => $data['identity_card_file_id'],
            'license_file_id' => $data['license_file_id'],
        ]);
    }

    public function update(array $data, Biker $biker)
    {
        $biker->update($data);
    }

}
