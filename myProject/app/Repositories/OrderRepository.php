<?php

namespace App\Repositories;

use App\Models\Biker;
use App\Models\Order\Order;
use Carbon\Carbon;

class OrderRepository
{

    /**
     * store new biker.
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store(array $data)
    {
        return Order::query()->create([
            'user_id' => $data['user_id'] ?? null,
            'cost' => $data['cost'],
            'description' => $data['description'],
            'status' => $data['status'],
            'payment_status' => $data['payment_status'],
        ]);
    }

    public function update(array $data, Biker $biker)
    {
        $biker->update($data);
    }

}
