<?php

namespace App\Repositories;

use App\Exceptions\File\UploadException;
use App\Models\File\File;
use App\Models\File\RootFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PHPUnit\Exception;

class FileRepository
{

    /**
     * store uploaded new file.
     *
     * @param UploadedFile $uploadedFile Uploaded File Object.
     * @param string       $path         File Uplaed apth.
     * @param string       $visibility   File Visibility.
     *
     * @return File
     * @throws \Exception
     */
    public function upload(UploadedFile $uploadedFile, $path, $visibility = File::PUBLIC): File
    {
        //TODO::add some condition for visibility
        $UploadedPath = $uploadedFile->store($path);
        $rootFile = RootFile::query()->create([
            'content_hash' => sha1($uploadedFile->getPathname()),
            'path' => $UploadedPath,
            'mime_type' => $uploadedFile->getMimeType(),
            'size' => $uploadedFile->getSize(),
        ]);

        if (empty($rootFile)) {
            throw new UploadException('Root file does not created');
        }

        $file = File::query()
            ->create([
                'name' => $uploadedFile->getClientOriginalName(),
                'extension' => $uploadedFile->getExtension(),
                'root_file_id' => $rootFile->id,
                'status' => $visibility,
                'uuid' => Str::uuid(),
            ]);


        if (empty($file)) {
            throw new UploadException('File does not created');
        }

        return $file;
    }

    /**
     * @param File   $file
     * @param string $status
     *
     * @return void
     */
    public function update(File $file, string $status)
    {
        $file->update(['status' => $status]);
    }


    public function delete(File $file)
    {
        DB::transaction();
        try {
            $similarFile = File::query()
                ->where('root_file_id', $file->root_file_id)
                ->count();

            $file->delete();

            if ($similarFile == 1) {
                Storage::disk('public')->delete($file->rootFile->path);

                $directory = dirname($file->rootFile->path);

                $file = Storage::disk('public')->files(Storage::disk('public')->path($directory));

                if (count($file) === 0) {
                    Storage::disk('public')->deleteDirectory($directory);
                }

                $file->rootFile->delete();
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function deleteAndUpdateFile(UploadedFile $uploadedFile, $file, $path)
    {
        if (Storage::disk(config('filesystems.default'))->exists($file->rootFile->path)) {
            Storage::disk(config('filesystems.default'))->delete($file->rootFile->path);
        }
        $UploadedPath = $uploadedFile->store($path);

        $rootFile = $file->rootFile->update(
            [
                'content_hash' => sha1($uploadedFile->getPathname()),
                'path' => $UploadedPath,
                'mime_type' => $uploadedFile->getMimeType(),
                'size' => $uploadedFile->getSize(),
            ]
        );

        if (empty($rootFile)) {
            throw new UploadException('Root file does not created');
        }

        $file->update(
            [
                'name' => $uploadedFile->getClientOriginalName(),
                'extension' => $uploadedFile->getExtension(),
                'uuid' => Str::uuid(),
            ]
        );

        if (empty($file)) {
            throw new UploadException('Root file does not created');
        }

        return $file;
    }
}
