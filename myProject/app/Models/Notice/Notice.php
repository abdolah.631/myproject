<?php

namespace App\Models\Notice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OwenIt\Auditing\Contracts\Auditable;

class Notice extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    public const TABLE = 'notices';
    public const TYPE_EMAIL = 'email';
    public const TYPE_SMS = 'sms';

    public static array $types = [
        self::TYPE_EMAIL,
        self::TYPE_SMS,
    ];

    protected $fillable = [
        'type',
        'slug',
        'is_active',
        'notice_template_id',
    ];

    /**
     * @return BelongsTo
     */
    public function noticeTemplate(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(NoticeTemplate::class);
    }


//    /**
//     * {@inheritdoc}
//     */
//    public function generateTags(): array
//    {
//        return [
//            'notice',
//        ];
//    }
}
