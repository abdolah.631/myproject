<?php

namespace App\Models\Notice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class NoticeTemplate extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    const TABLE = 'notice_templates';


    public const TYPE_PLAINTEXT = 'plaintext';
    public const TYPE_HTML = 'html';

    public static array $statusTypes = [
        self::TYPE_HTML,
        self::TYPE_PLAINTEXT,
    ];

    protected $fillable = [
        'name',
        'date',
        'title',
        'plaintext',
        'html',
        'type',
    ];

    public function notice()
    {
    return $this->hasOne(Notice::TABLE);
    }
}
