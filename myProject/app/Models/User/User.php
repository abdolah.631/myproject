<?php

namespace App\Models\User;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Biker;
use App\Models\Order\Order;
use App\Models\Order\OrderBiker;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Order\Contracts\ProvidesInvoiceInformation;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Scout\Searchable;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens,
        HasFactory,
        Notifiable,
        HasRoles,
        HasPermissions,
        Billable,
        Searchable;

    public const TABLE = 'users';

    public const MALE = 'male';
    public const FEMALE = 'female';
    public const OTHER = 'other';

    public const SINGLE = 'single';
    public const MARRIAGE = 'marriage';

    public const GENDER = [
        self::FEMALE,
        self::MALE,
        self::OTHER,
    ];

    public const MARRIAGE_TYPE = [
        self::SINGLE,
        self::MARRIAGE,
    ];

    protected $table = self::TABLE;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'born_date',
        'nation_code',
        'address',
        'gender',
        'marriage_type',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'born_date' => 'datetime',
    ];

    /**
     * user has only one biker.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function biker(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Biker::class, 'user_id', 'id');
    }

    /**
     * @return HasManyThrough
     */
    public function bikerOrder(): HasManyThrough
    {
        return $this->hasManyThrough(OrderBiker::class, Biker::class);
    }

    /**
     * user has many order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return HasMany
     */
    public function hasActiveOrder(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->order()
            ->whereIn('status', [
                Order::STATUS_BIKER_CHOSEN,
                Order::STATUS_LOAD_PICKUP,
            ]);
    }


    /**
     * get is biker.
     *
     * @return Attribute
     */
    public function isBiker(): Attribute
    {
        return Attribute::make(function ($value) {
            if ($this->biker()->exists()) {
                return true;
            }

            return false;
        });
    }

    /**
     * get is biker.
     *
     * @return bool|Attribute
     */
    public function isAdmin(): Attribute|bool
    {
        if ($this->hasRole(Role::ADMIN)) {
            return true;
        }

        return false;
    }

    /**
     * get is biker.
     *
     * @return bool
     */
    public function isUser(): bool
    {
        if ($this->hasRole(Role::USER)) {
            return true;
        }

        return false;
    }

    /**
     * @return Attribute
     */
    public function emailVerifiedAt(): Attribute
    {
        return Attribute::get(
            get: fn($value) => $value ? 'verified' : 'unverified'
        );
    }

    /**
     * @return UserFactory
     */
    protected static function newFactory(): UserFactory
    {
        return new UserFactory();
    }

    /**
     * @return Attribute
     */
    public function gender(): Attribute
    {
        return Attribute::get(
            get: fn($value) => $value == 'female' ? 'زن' : 'مرد'
        );
    }

}
