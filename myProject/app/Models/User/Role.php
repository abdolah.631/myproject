<?php

namespace App\Models\User;


use App\Constants\RolePermissions\AdminPermissions;
use App\Constants\RolePermissions\BikerPermissions;
use App\Constants\RolePermissions\UserPermissions;

class Role extends \Spatie\Permission\Models\Role
{
    public const USER = 'user';
    public const ADMIN = 'admin';
    public const BIKER = 'biker';

    public const CUSTOM_ROLES = [
        UserPermissions::class => self::USER,
        AdminPermissions::class => self::ADMIN,
        BikerPermissions::class => self::BIKER,
    ];

    /**
     * @param $name
     * @param $guards
     *
     * @return Role|\Spatie\Permission\Models\Role
     */
    public static function getRoleByName($name, $guards = null): \Spatie\Permission\Models\Role|Role
    {
        $guards = $guards ?? config('auth.defaults.guard');

        return self::query()
            ->where(
                [
                    ['name', $name],
                    ['guard_name', $guards],
                ]
            )
            ->first();
    }
}
