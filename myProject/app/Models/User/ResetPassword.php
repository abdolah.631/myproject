<?php

namespace App\Models\User;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
    use HasFactory;

    const INTERVAL_RESEND = 2;

    protected $primaryKey = 'email';

    protected $table = 'password_resets';

    public $timestamps = false;


    protected $fillable = [
        'email',
        'token',
        'expired_at',
        'created_at',
    ];

    protected $casts = [
        'expired_at' => 'datetime',
        'created_at' => 'datetime',
    ];

    public function isTokenExpired()
    {
        $hours = $this->getCreatedAt()->diffInHours(Carbon::now(), false);

        return $hours >= 1;
    }
}
