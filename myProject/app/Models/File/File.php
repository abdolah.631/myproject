<?php

namespace App\Models\File;

use Database\Factories\FileFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class File extends Model
{
    use HasFactory;

    const PUBLIC = 'public';
    const PRIVATE = 'private';
    const DISABLED = 'disabled';

    const TABLE = 'files';
    public $table = self::TABLE;

    public static array $statusTypes = [
        self::PUBLIC,
        self::PRIVATE,
        self::DISABLED,
    ];

    protected $fillable = [
        'name',
        'extension',
        'root_file_id',
        'status',
        'uuid',
    ];


    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * @return BelongsTo
     */
    public function rootFile(): BelongsTo
    {
        return $this->belongsTo(RootFile::class,);
    }

    protected static function newFactory()
    {
        return new FileFactory();
    }
}
