<?php

namespace App\Models\File;

use Database\Factories\RootFileFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RootFile extends Model
{
    use HasFactory;



    protected $fillable = [
        'content_hash',
        'path',
        'mime_type',
        'size',
    ];

    public function File()
    {
        return $this->hasOne(File::class);
    }

    protected static function newFactory()
    {
        return new RootFileFactory();
    }
}
