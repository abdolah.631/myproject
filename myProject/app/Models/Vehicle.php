<?php

namespace App\Models;

use App\Observers\VehicleObserver;
use Database\Factories\VehicleFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Vehicle extends Model
{
    use HasFactory;
    use HasSlug;

    public const TABLE = 'vehicles';
    protected $table = self::TABLE;

    protected $fillable = [
        'name',
        'biker_id',
        'is_active',
        'slug',
        'color',
        'vehicle_card_file_id',
        'year_of_construction',
    ];

    public static function boot(): void
    {
        parent::boot();
        self::observe(VehicleObserver::class);
    }


    /**
     * @return SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * @return Attribute
     */
    public function isActive(): Attribute
    {
        return Attribute::get(
            get: fn($value) => $value ? 'active' : 'inactive'
        );
    }

    /**
     * @return VehicleFactory
     */
    public static function newFactory()
    {
        return new VehicleFactory();
    }

    public function biker()
    {
        return $this->belongsTo(Biker::class, 'biker_id', 'id');
    }


}
