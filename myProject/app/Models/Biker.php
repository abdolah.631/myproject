<?php

namespace App\Models;

use App\Models\File\File;
use App\Models\Order\OrderBiker;
use App\Models\Order\OrderRate;
use App\Models\User\User;
use Database\Factories\BikerFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use LaravelIdea\Helper\App\Models\Order\_IH_OrderBiker_QB;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class Biker extends Model
{
    use HasFactory, HasPermissions, HasRoles;

    public const TABLE = 'bikers';
    public const BLOCKED = 'blocked';
    public const NEED_ACTIVATION = 'need-Activation';
    const BIKER_DOCUMENT_PATH = '/biker_document';

    public const STATUS = [
        self::BLOCKED,
        self::NEED_ACTIVATION,
    ];

    protected $table = self::TABLE;

    protected $fillable = [
        'user_id',
        'status',
        'is_active',
        'identity_card_file_id',
        'license_file_id',
        'registration_date',
    ];

    protected $casts = [
        'registration_date' => 'datetime',
    ];

    protected static function newFactory(): BikerFactory
    {
        return new BikerFactory();
    }

    /**
     * belong to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function identityCard(): BelongsTo
    {
        return $this->belongsTo(File::class, 'identity_card_file_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function license(): BelongsTo
    {
        return $this->belongsTo(File::class, 'license_file_id', 'id');
    }


    public function IsActive(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value ? 'true' : 'false',
        );
    }

    /**
     * get all vehicles of biker.
     *
     * @return HasMany
     */
    public function vehicle(): HasMany
    {
        return $this->hasMany(Vehicle::class, 'biker_id', 'id');
    }

    /**
     * get all orders of biker.
     *
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(OrderBiker::class);
    }

    /**
     * get all rates  of order,s biker.
     *
     * @return HasMany
     */
    public function orderRates(): HasMany
    {
        return $this->hasMany(OrderRate::class);
    }

    /**
     * get all orders of biker.
     *
     * @return HasMany
     */
    public function bikerOrders(): HasMany
    {
        return $this->hasMany(OrderBiker::class);
    }

    /**
     * get active vehicle of biker.
     *
     * @return HasMany
     */
    public function activeVehicle(): HasMany
    {
        return $this->vehicle()->where('is_active', true);
    }

    /**
     * @return HasMany|_IH_OrderBiker_QB
     */
    public function isBikerAcceptingService(): HasMany|_IH_OrderBiker_QB
    {
        return $this->bikerOrders()->where('status', OrderBiker::STATUS_ACCEPT);
    }

}
