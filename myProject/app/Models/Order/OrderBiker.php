<?php

namespace App\Models\Order;

use App\Models\Biker;
use Database\Factories\OrderBikerFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderBiker extends Model
{
    use HasFactory;

    public const STATUS_CANCEL = 'cancel';
    public const STATUS_ACCEPT = 'accept';
    public const STATUS_COMPLETION = 'completed';

    public static array $status = [
        self::STATUS_CANCEL,
        self::STATUS_COMPLETION,
        self::STATUS_ACCEPT,
    ];

    protected $fillable = [
        'order_id',
        'biker_id',
        'status',
    ];

    /**
     * @return BelongsTo
     */
    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return BelongsTo
     */
    public function biker(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Biker::class);
    }

    public static function newFactory(): OrderBikerFactory
    {
        return new OrderBikerFactory();
    }
}
