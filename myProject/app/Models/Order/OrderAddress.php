<?php

namespace App\Models\Order;

use Database\Factories\OrderAddressFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderAddress extends Model
{
    use HasFactory;

    protected $appends = ['full_name'];

    protected $fillable = [
        'user_id',
        'order_id',
        'first_name',
        'last_name',
        'recipient_phone',
        'sender_phone',
        'sender_address',
        'recipient_address',
        'begin_latitude',
        'begin_longitude',
        'destination_latitude',
        'destination_longitude',
    ];

    /**
     * @return BelongsTo
     */
    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . '-' . $this->last_name;
    }

    public static function newFactory()
    {
        return new OrderAddressFactory();
    }

}
