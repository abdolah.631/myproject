<?php

namespace App\Models\Order;

use App\Models\Payment;
use Database\Factories\OrderFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public const STATUS_CANCEL_ORDER = 'cancel-order';
    public const STATUS_SEARCH_BIKER = 'search-biker';
    public const STATUS_BIKER_CHOSEN = 'biker-choices';
    public const STATUS_LOAD_PICKUP = 'load-pickup';
    public const STATUS_LOAD_RECEIVE = 'load-receive';

    public const PAYMENT_PAiD = 'paid';
    public const PAYMENT_UNPAID = 'unpaid';

    public static array $status = [
        self::STATUS_SEARCH_BIKER,
        self::STATUS_BIKER_CHOSEN,
        self::STATUS_LOAD_PICKUP,
        self::STATUS_LOAD_RECEIVE,
        self::STATUS_CANCEL_ORDER,
    ];

    public static array $paymentStatus = [
        self::PAYMENT_PAiD,
        self::PAYMENT_UNPAID,
    ];
    protected $fillable = [
        'user_id',
        'description',
        'cost',
        'status',
        'payment_status',
    ];

    public function addresses()
    {
        return $this->hasOne(OrderAddress::class);
    }

    public function rate()
    {
        return $this->hasOne(OrderRate::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function biker()
    {
        return $this->hasMany(OrderBiker::class);
    }

    public static function newFactory()
    {
        return new OrderFactory();
    }

    public function isFreeOrder()
    {
        return $this->hasMany(OrderBiker::class)->where('status' , OrderBiker::STATUS_ACCEPT);
    }


}
