<?php

namespace App\Listeners;

use App\Jobs\SendNotice;
use http\Url;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Carbon;

class SendEmailVerificationListener
{
    use Dispatchable;
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param object $event
     *
     * @return void
     */
    public function handle(Registered $event)
    {
        $verifyLink = \Illuminate\Support\Facades\URL::temporarySignedRoute(
            'register.verify',
            Carbon::now()->addMinutes(\Config::get('auth.verification.expire', 60)),
            [
                'user' => $event->user->id,
                'hash' => sha1($event->user->email),
            ]
        );

        SendNotice::dispatch($event->user->email, $verifyLink);
    }
}
