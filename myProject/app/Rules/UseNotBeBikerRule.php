<?php

namespace App\Rules;

use App\Models\Biker;
use Illuminate\Contracts\Validation\Rule;

class UseNotBeBikerRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $userId
     * @return bool
     */
    public function passes($attribute, $userId)
    {
        return !Biker::query()->where('user_id', $userId)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'User has been Biker before.';
    }
}
