<?php

namespace App\Rules;

use App\Models\Biker;
use Illuminate\Contracts\Validation\Rule;

class BikerJustAndOnlyJustHaveOneActiveVehicle implements Rule
{
    private Biker $biker;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($biker)
    {
        $this->biker = $biker;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value == "0") {
            if (empty($this->biker?->activeVehicle()?->count())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Bikers should have only and just only one Active vehicle';
    }
}
