<?php


use App\Constants\Permissions;
use App\Http\Controllers\VehicleController;

Route::get('all-vehicles', [VehicleController::class, 'allVehicles'])
    ->middleware(['auth:web', 'permission:' . Permissions::MANAGE_VEHICLE])
    ->name('vehicle.all-vehicles');

Route::resource('{biker}/vehicles', VehicleController::class )
->middleware(['auth:web', 'permission:' . Permissions::MANAGE_VEHICLE]);


