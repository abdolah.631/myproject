<?php

use App\Http\Controllers\MollieController;


Route::get('mollie-payment', [MollieController::class, 'preparePayment'])
    ->name('mollie.payment');

Route::get('payment-success', [MollieController::class, 'paymentSuccess'])
    ->name('payment.success');
