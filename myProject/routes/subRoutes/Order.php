<?php


use App\Constants\Permissions;
use App\Http\Controllers\OrderController;


Route::middleware(['auth:web', 'permission:' . Permissions::MANAGE_ORDERS])
    ->group(function () {
        Route::resource('orders', OrderController::class);
    });

Route::get('/make-order', function () {
    return view('homePanel.makeOrder');
})->name('make.order');


Route::resource('orders', OrderController::class)
    ->middleware(['auth:web', 'permission:' . Permissions::MANAGE_ORDERS]);


Route::post('order/create', [OrderController::class, 'storeOrder'])
    ->middleware('auth:web')
    ->name('order.create');

Route::get('services', [OrderController::class, 'getServices'])
    ->middleware('auth:web')
    ->name('services');

Route::get('accepted-services', [OrderController::class, 'getAcceptedServices'])
    ->middleware('auth:web')
    ->name('getAcceptedServices');

Route::get('receive-load/{order}', [OrderController::class, 'receiveLoad'])
    ->middleware('auth:web')
    ->name('receive.load');

Route::post('change-order-status/{order}', [OrderController::class, 'changeOrderStatus'])
    ->middleware('auth:web')
    ->name('change.order.status');

Route::post('order-rate', [OrderController::class, 'orderRate'])
//    ->middleware('auth:web')
    ->name('order.rate');
