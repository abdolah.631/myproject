<?php

use App\Http\Controllers\PaymentController;


Route::any('payment/verify', [PaymentController::class, 'paymentVerify'])
    ->name('payment.verify');

Route::get('payment/{order}', [PaymentController::class, 'payment'])
    ->name('payment');



