<?php


use App\Constants\Permissions;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\UserController;

Auth::routes();

Route::get('register/verify/{user}', [RegisterController::class, 'verify'])
    ->name('register.verify');
Route::post('reset-password', [ResetPasswordController::class, 'resetPasswordEmail'])
    ->name('reset.password.email');


Route::resource('/users', UserController::class)
    ->middleware(['auth:web', 'permission:' . Permissions::MANAGE_USER]);

Route::get('profile', [UserController::class, 'profile'])->name('user.profile');

Route::get('user-order', [UserController::class, 'userOrder'])->name('user.order');

Route::get('user-cansel-service/{order}', [UserController::class, 'userCanselService'])->name('user.cansel.service');
