<?php


use App\Http\Controllers\FileController;

Route::prefix('cdn/')
    ->group(function (){
    Route::resource('files', FileController::class)
        ->middleware(['auth:web', 'permission:' . \App\Constants\Permissions::MANAGE_FILES])
        ->except('update');

    Route::get('files/download/{file}', [FileController::class, 'download'])
        ->name('file.download');

});
