<?php


use App\Constants\Permissions;
use App\Http\Controllers\BikerController;

Route::resource('/bikers', BikerController::class)
    ->middleware(['auth:web', 'permission:' . Permissions::MANAGE_BIKER]);


//Route::post('/bikers/add-biker/{user}', [BikerController::class , 'addBiker'])->name('biker.add');

Route::get('/bikers/create-biker/{user}', [BikerController::class, 'create'])
    ->middleware(['auth:web', 'permission:' . Permissions::CREATE_BIKER])
    ->name('biker.create');

Route::post('bikers/search-biker', function () {
    return 1;
})->name('search.biker');


Route::get('biker/{biker}/order/{order}', [BikerController::class, 'storeBikerOrder'])
    ->middleware(['auth:web', 'permission:'. Permissions::STORE_NEW_BIKER_ORDER])
    ->name('biker.order');

Route::get('biker-service', [BikerController::class, 'showAcceptedService'])
    ->middleware(['auth:web', 'permission:'. Permissions::SHOW_ACCEPTED_SERVICE])
    ->name('show.accepted.service');

Route::get('cansel-service/{order}', [BikerController::class, 'cancelService'])
    ->middleware(['auth:web', 'permission:'. Permissions::CANCEL_SERVICE])
    ->name('cansel.service');
