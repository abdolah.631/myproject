<?php

use App\Http\Controllers\BikerController;
use App\Http\Controllers\MollieController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

foreach (\Illuminate\Support\Facades\File::allFiles(__DIR__ . '/subRoutes') as $routeFile){
    require $routeFile->getPathname();
};


//home panel
Route::get('/', function () {
    return view('homePanel.home');
})->name('home');



//admin panel
Route::get('/admin/dashboard', [App\Http\Controllers\HomeController::class, 'index'])
    ->middleware('auth:web')
    ->name('dashboard');





//pusher
Route::get('/messages', function (){
    return view('message');
});

Route::post('/send', [BikerController::class, 'searchBiker'])
    ->name('send.search.biker');

Route::get('/form',function (){
  return view('form');
});
